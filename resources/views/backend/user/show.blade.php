@extends('backend.layout.master')
@section('content-title')
    USER: {{ $user->name }}
@endsection

@section('backend.custom-content')
<div class="content">
    <div class="row">
        <div class="col-md-4">
            @component('backend.layout._box')
                @slot('bodyClass', 'no-padding')
                @slot('title', 'User Detail')
                {{-- @slot('footer', 'User Detail') --}}
                <table class="table table-stripped">
                    <tr>
                        <th>Name</th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                    </tr>
                </table>

            @endcomponent
        </div>

        <div class="col-md-4">
            @component('backend.layout._box')
                @slot('bodyClass', 'no-padding')
                @slot('title', 'Active Class')
                <table class="table table-stripped">
                    @foreach ($activeProducts as $product)
                    <tr>
                        <td>
                            <strong>{{ $product->name }}</strong> <br>
                            {{ formatDate($product->start_at) }} - {{ formatDate($product->end_at) }}
                            {{ $product->location_name }}
                        </td>
                    </tr>
                    @endforeach
                </table>
            @endcomponent
            @component('backend.layout._box')
                @slot('bodyClass', 'no-padding')
                @slot('title', 'Upcoming Class')
                <table class="table table-stripped">
                    @foreach ($upcomingProducts as $product)
                    <tr>
                        <td>
                            <strong>{{ $product->name }}</strong> <br>
                            {{ formatDate($product->start_at) }} - {{ formatDate($product->end_at) }} <br>
                            {{ $product->location_name }}
                        </td>
                    </tr>
                    @endforeach
                </table>
            @endcomponent
        </div>

        <div class="col-md-4">
            <h4>Order History</h4>
            <ul class="timeline">
                @foreach ($user->orders as $order)
                  <li>
                        <i class="fa fa-file-text-o bg-blue"></i>
                        <div class="timeline-item">
                            <h3 class="timeline-header">{{ formatDate($order->created_at) }}</h3>
                            <div class="timeline-body">
                                @foreach ($order->products as $product)
                                    <p><strong>{{ $product->pivot->name }}</strong> <br>
                                        IDR {{ formatCurrency($product->pivot->price) }} <br>
                                        {{ formatDate($product->pivot->start_at) }} -
                                        {{ formatDate($product->pivot->end_at) }}
                                    </p>
                                @endforeach
                            </div>
                        <div class="timeline-footer">
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection

@push('footer-scripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush
@extends('backend.layout.master', [
    'left_content_size' => 4,
    'right_content_size' => 3,
])

@section('content-title')
    CATEGORY <small>of Post</small>
@endsection

@section('content-alt-header', 'Category List')

@section('content-alt-body')
    @if ($parentCategories->isEmpty())
        @include('common.datalist.no-data')
    @else
        <table id="" class="table table-bordered table-striped table-hover table-leveled">
            <thead>
               <tr>
                   <th class="text-left">Name</th>
                   <th width="100"></th>
               </tr>
           </thead>
           <tbody>
                @foreach ($parentCategories as $parentCategory)
                    <tr class="level-1">
                        <td class="leveled"><strong>{{ $parentCategory->name }}</strong></td>
                        <td class="text-right">
                            @component('common.datalist.button-edit')
                                @slot('text', '')
                                @slot('route', route('category.edit', [
                                                        $parentCategory,
                                                        'page' => $_GET['page'] ?? 1
                                                     ]
                                               )
                                )
                            @endcomponent

                            @component('common.datalist.button-remove')
                                @slot('text', '')
                                @slot('route', route('category.destroy', $parentCategory))
                            @endcomponent
                        </td>
                    </tr>
                    @foreach ($parentCategory->childs as $categoryLevel2)
                        <tr class="level-2">
                            <td class="leveled">{{ $categoryLevel2->name }}</td>
                            <td class="text-right">
                                @component('common.datalist.button-edit')
                                    @slot('text', '')
                                    @slot('route', route('category.edit', [
                                                            $categoryLevel2,
                                                            'page' => $_GET['page'] ?? 1
                                                         ]
                                                   )
                                    )
                                @endcomponent

                                @component('common.datalist.button-remove')
                                    @slot('text', '')
                                    @slot('route', route('category.destroy', $categoryLevel2))
                                @endcomponent
                            </td>
                        </tr>
                        @foreach ($categoryLevel2->childs as $categoryLevel3)
                            <tr class="level-3">
                                <td class="leveled">{{ $categoryLevel3->name }}</td>
                                <td class="text-right">
                                    @component('common.datalist.button-edit')
                                        @slot('text', '')
                                        @slot('route', route('category.edit', [
                                                                $categoryLevel3,
                                                                'page' => $_GET['page'] ?? 1
                                                             ]
                                                       )
                                        )
                                    @endcomponent

                                    @component('common.datalist.button-remove')
                                        @slot('text', '')
                                        @slot('route', route('category.destroy', $categoryLevel3))
                                    @endcomponent
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                @endforeach
            </tbody>
        </table>
    @endif
@endsection

@section('content-alt-form')
    @include('backend.category._form')
@endsection
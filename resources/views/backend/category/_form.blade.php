<div class="box-header with-border">

    @if (URL::current() == route('category.index'))

        <h3 class="box-title">Create New Category</h3>

    @elseif (URL::current() == route('category.edit', $category_edit))

        <h3 class="box-title">Edit Category</h3>

    @endif

</div>

@if (URL::current() == route('category.index'))

    <form action="{{ route('category.store') }}" method="post">

@elseif (URL::current() == route('category.edit', $category_edit))

    <form action="{{ route('category.update', $category_edit) }}" method="post">

        {{ method_field('PATCH') }}

        <input type="hidden" name="page" id="name" value="{{ $_GET['page'] }}">

        @endif

        {{ csrf_field() }}

        <div class="box-body">
            <div class="form-group {{ $errors->has('name') ? 'has-error has-feedback' : '' }}">
                <label for="name">
                    Name @include('common.form.label-required-field')
                </label>

                <input type="text" class="form-control" id="name" name="name"
                       value="{{ old('name') ?: (isset($category_edit->name) ? $category_edit->name : '') }}">

                @if ($errors->has('name'))
                    @include('common.form.input-error-message', ['message' => $errors->first('name')])
                @endif
            </div>

            <div class="form-group {{ $errors->has('parent_id') ? 'has-error has-feedback' : '' }}">
                <label for="parent_id">
                    Parent @include('common.form.label-required-field')
                </label>

                <select class="form-control select2" id="parent_id" name="parent_id">
                    <option value="">(no parent)</option>

                    @foreach ($parentCategories as $parentCategory)
                        <option class="level-1" value="{{ $parentCategory->id }}"
                                {{ (old('parent_id') == $parentCategory->id) ? 'selected' : '' }}
                                {{ isset($category_edit) ?
                                        (($category_edit->parent_id == $parentCategory->id) ? 'selected' : '') : '' }}>

                            {{ $parentCategory->name }}
                        </option>
                        @foreach ($parentCategory->childs as $subcategory)
                            <option class="level-2" value="{{ $subcategory->id }}"
                                {{ (old('parent_id') == $subcategory->id) ? 'selected' : '' }}
                                {{ isset($category_edit) ?
                                        (($category_edit->parent_id == $subcategory->id) ? 'selected' : '') : '' }}>

                                — {{ $subcategory->name }}
                            </option>
                        @endforeach
                        {{-- <option value="{{ $parent_category->id }}"

                                {{ (old('parent_id') == $parent_category->id) ? 'selected' : '' }}

                                {{ isset($category_edit) ?
                                        (($category_edit->parent_id == $parent_category->id) ? 'selected' : '') : '' }}>

                            {{ $parent_category->name }}
                        </option> --}}
                    @endforeach
                </select>

                @if ($errors->has('parent_id'))
                    @include('components.form.input-error-message', ['message' => $errors->first('parent_id')])
                @endif
            </div>

            <div class="form-group">
                @component('common.form.button-save-new-or-save-changes')
                    @slot('route_create', route('category.index'))
                    @slot('route_edit', route('category.edit', ($category_edit ?? 0)))
                @endcomponent

                @component('common.form.button-cancel')
                    @slot('route_edit', route('category.edit', ($category_edit ?? 0)))
                    @slot('route_redirect', route('category.index', ['page' => $_GET['page'] ?? 1]))
                @endcomponent
            </div>
        </div>
    </form>


@push('header-scripts')
    <link rel="stylesheet" href="{{ asset('assets/adminlte/bower_components/select2/dist/css/select2.min.css') }}">
@endpush

@push('footer-scripts')
    <script src="{{ asset('assets/adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script>
        $(function () {
            $('.select2').select2();
        });
    </script>
@endpush
@extends('backend.layout.master')

@section('content-title', 'POST')

@section('content-header', 'Create new Post')

@section('content-body')
    <div class="col-md-12">
        <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            @include('backend.post._form')
        </form>
    </div>
@endsection

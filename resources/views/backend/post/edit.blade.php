@extends('backend.layout.master')

@section('content-title', 'POST')

@section('content-header', 'Edit Post')

@section('content-body')
    <div class="col-md-12">
        <form action="{{ route('post.update', $post) }}" method="post" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}

            @include('backend.post._form')
        </form>
    </div>
@endsection

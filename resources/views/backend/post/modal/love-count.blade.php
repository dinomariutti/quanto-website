<div class="modal fade" id="post-{{ $post->id }}-likes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Liked users list of:</h4>
                <h3 class="modal-title" id="myModalLabel">
                    {{ $post->title }}
                </h3>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                        <tr>
                            <th class="text-center">User Name / Anonymous IP Address</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($post->likes()->orderBy('created_at', 'desc')->get() as $like)
                            <tr>
                                <td>
                                    @if (isset($like->user))
                                        {{ $like->user->name }} <br>

                                        <span class="fa fa-envelope-o"></span>
                                        <a href="mailto:{{ $like->user->email }}">
                                            {{ $like->user->email }}
                                        </a>
                                    @else
                                        {{ $like->ip_address }}
                                    @endif
                                </td>
                                <th class="text-right">
                                    @component('common.datalist.button-remove')
                                        @slot('text', 'remove')
                                        @slot('icon', '')
                                        @slot('size', 'xs')
                                        @slot('route', route('like.destroy', $like))
                                    @endcomponent
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <i class="fa fa-close"></i> Close
                </button>
            </div>
        </div>
    </div>
</div>
<div class="active tab-pane" id="post-content">
    
    <h4>Excerpt</h4>

    {!! $post->excerpt !!}

    <br><br>

    <h4>Main Content</h4>

    {!! $post->body !!}

</div>
<div class="tab-pane" id="post-social-seo">
    
    <h4>Open Graph - Facebook, LinkedIn, Google+</h4>

    <table class="table">
        <tr>
            <td width="150px;">Title</td>
            <td>{{ $post->social->open_graph_title or '-' }}</td>
        </tr>
        <tr>
            <td>Site Name</td>
            <td>{{ $post->social->open_graph_sitename or '-' }}</td>
        </tr>
        <tr>
            <td>Post URL</td>
            <td>{{ $post->social->open_graph_post_url or '-' }}</td>
        </tr>
        <tr>
            <td>Description</td>
            <td>{{ $post->social->open_graph_description or '-' }}</td>
        </tr>
    </table>

    <br><br>

    <h4>Twitter</h4>

    <table class="table">
        <tr>
            <td width="150px;">Card Type</td>
            <td>{{ $post->social->twitter_card_type or '-' }}</td>
        </tr>
        <tr>
            <td>Title</td>
            <td>{{ $post->social->twitter_title or '-' }}</td>
        </tr>
        <tr>
            <td>Description</td>
            <td>{{ $post->social->twitter_description or '-' }}</td>
        </tr>
    </table>

</div>
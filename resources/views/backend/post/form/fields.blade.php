<div class="form-group {{ $errors->has('title') ? 'has-error has-feedback' : '' }}">
    <label for="title">
        Title @include('common.form.label-required-field')
    </label>

    <input type="text" class="form-control" id="title" name="title"
            value="{{ old('title') ?: (isset($post->title) ? $post->title : '') }}">

    @if ($errors->has('title'))
        @include('common.form.input-error-message', ['message' => $errors->first('title')])
    @endif
</div>

<div class="form-group {{ $errors->has('category_id') ? 'has-error has-feedback' : '' }}">
    <label for="category_id">
        Category @include('common.form.label-required-field')
    </label>

    <select class="form-control select2" id="category_id" name="category_id">
        @foreach ($parent_categories as $parent)
            <option class="level-1" value="{{ $parent->id }}"
                    {{ (old('category_id') == $parent->id) ? 'selected' : '' }}
                    
                    {{ isset($post->categories) ? 
                        ((in_array($parent->id, $post->categories->pluck('id')->toArray()) ? 
                            'selected' : '')) : '' }}>

                {{ $parent->name }}
            </option>
            @foreach ($parent->childs as $child)
                <option class="level-2" value="{{ $child->id }}"
                    {{ (old('category_id') == $child->id) ? 'selected' : '' }}
                    
                    {{ isset($post->categories) ? 
                        ((in_array($child->id, $post->categories->pluck('id')->toArray()) ? 
                            'selected' : '')) : '' }}>

                    — {{ $child->name }}
                </option>
            @endforeach
        @endforeach
    </select>

    @if ($errors->has('category_id'))
        @include('common.form.input-error-message', ['message' => $errors->first('category_id')])
    @endif

    <span class="help-block">
        Can not spot the Category you're looking for?

        @component('common.buttons.create-new')
            @slot('style', 'display: inline;')
            @slot('alignment' , '')
            @slot('route', route('category.index'))
            @slot('color', 'info')
            @slot('size', 'xs')
            @slot('text', 'New Category')
        @endcomponent
    </span>
</div>

<div class="form-group {{ $errors->has('body') ? 'has-error has-feedback' : '' }}">
    <label for="body">
        Content @include('common.form.label-required-field')
    </label>

    <textarea class="form-control" id="body" name="body"
                rows="5">{{ old('body') ?: (isset($post->body) ? $post->body : '') }}</textarea>

    @if ($errors->has('body'))
        @include('common.form.input-error-message', ['message' => $errors->first('body')])
    @endif
</div>

<div class="form-group {{ $errors->has('excerpt') ? 'has-error has-feedback' : '' }}">
    <label for="excerpt">
        Excerpt @include('common.form.label-required-field')
    </label>

    <textarea class="form-control" id="excerpt" name="excerpt"
                rows="5">{{ old('excerpt') ?: (isset($post->excerpt) ? $post->excerpt : '') }}</textarea>

    @if ($errors->has('excerpt'))
        @include('common.form.input-error-message', ['message' => $errors->first('excerpt')])
    @endif
</div>

<div class="form-group {{ $errors->has('tag_id') ? 'has-error has-feedback' : '' }}">
    <label for="tag_id">
        Tag 
    </label>

    <select class="form-control select2" id="tag_id" name="tag_id[]" multiple>
        @foreach ($tags as $tag)
            <option value="{{ $tag->id }}" 
                
                    {{ old('tag_id') ? (in_array($tag->id, old('tag_id')) ? 'selected' : '') : '' }}

                    {{ isset($post->tags) ? 
                        ((in_array($tag->id, $post->tags->pluck('id')->toArray()) ? 
                            'selected' : '')) : '' }}>

                {{ $tag->name }}
            </option>
        @endforeach
    </select>

    @if ($errors->has('tag_id'))
        @include('common.form.input-error-message', ['message' => $errors->first('tag_id')])
    @endif

    <span class="help-block">
        Can not spot the Tag you're looking for?

        @component('common.buttons.create-new')
            @slot('style', 'display: inline;')
            @slot('alignment' , '')
            @slot('route', route('tag.index'))
            @slot('color', 'info')
            @slot('size', 'xs')
            @slot('text', 'New Tag')
        @endcomponent
    </span>
</div>

<div class="form-group {{ $errors->has('images') ? 'has-error has-feedback' : '' }}
        {{ $errors->has('images.*') ? 'has-error has-feedback' : '' }}">

    <label for="images">
        Featured Image @include('common.form.label-required-field')
    </label>

    <input type="file" name="images[][image]" multiple class="form-control">

    @if ($errors->has('images'))
        @include('common.form.input-error-message', ['message' => $errors->first('images')])
    @endif

    <span class="help-block">
        Acceptable types are PNG or JPG. Max size 1 MB in 1200 x 450 dimension.
    </span>

    @if ($errors->has('images.*'))
        @foreach ($errors->get('images.*') as $image)
            @include('common.form.input-error-message', ['message' => $image[0]])
        @endforeach
    @endif
</div>

<fieldset id="seo" style="margin-bottom: 20px;">
    <div class="form-group {{ $errors->has('meta_description') ? 'has-error has-feedback' : '' }}">
        <legend>SEO</legend>
    
        <label for="meta_description">
            Meta Description 
        </label>
    
        <textarea class="form-control" id="meta_description" name="meta_description"
                    rows="5">{{ old('meta_description') ?: 
                                (isset($post->meta_description) ? 
                                $post->meta_description : '') }}</textarea>
    
        @if ($errors->has('meta_description'))
            @include('common.form.input-error-message', [
                'message' => $errors->first('meta_description')
            ])
        @endif
    </div>
</fieldset>

<fieldset id="social-seo">
    <legend>Social SEO</legend>

    <h4>Open Graph - Facebook, LinkedIn, Google+</h4>

    <div class="form-group {{ $errors->has('open_graph_title') ? 'has-error has-feedback' : '' }}">
        <label for="open_graph_title">Title</label>
    
        <input type="text" class="form-control" id="open_graph_title" name="open_graph_title"
            value="{{ old('open_graph_title') ?: 
                    (isset($post->social->open_graph_title) ? $post->social->open_graph_title : '') }}">

        @if ($errors->has('open_graph_title'))
            @include('common.form.input-error-message', [
                'message' => $errors->first('open_graph_title')
            ])
        @endif
    </div>

    <div class="form-group {{ $errors->has('open_graph_sitename') ? 'has-error has-feedback' : '' }}">
        <label for="open_graph_sitename">Site Name</label>
    
        <input type="text" class="form-control" id="open_graph_sitename" name="open_graph_sitename"
            value="{{ old('open_graph_sitename') ?: 
                    (isset($post->social->open_graph_sitename) ? $post->social->open_graph_sitename : '') }}">

        @if ($errors->has('open_graph_sitename'))
            @include('common.form.input-error-message', [
                'message' => $errors->first('open_graph_sitename')
            ])
        @endif
    </div>

    <div class="form-group {{ $errors->has('open_graph_post_url') ? 'has-error has-feedback' : '' }}">
        <label for="open_graph_post_url">Post URL</label>
    
        <input type="url" class="form-control" id="open_graph_post_url" name="open_graph_post_url" 
            placeholder="http://"
            value="{{ old('open_graph_post_url') ?: 
                    (isset($post->social->open_graph_post_url) ? $post->social->open_graph_post_url : '') }}">

        @if ($errors->has('open_graph_post_url'))
            @include('common.form.input-error-message', [
                'message' => $errors->first('open_graph_post_url')
            ])
        @endif
    </div>

    <div class="form-group {{ $errors->has('open_graph_description') ? 'has-error has-feedback' : '' }}">
        <label for="open_graph_description">Description</label>
    
        <textarea class="form-control" id="open_graph_description" name="open_graph_description"
                    rows="3">{{ old('open_graph_description') ?: 
                                (isset($post->social->open_graph_description) ? 
                                $post->social->open_graph_description : '') }}</textarea>
    
        @if ($errors->has('open_graph_description'))
            @include('common.form.input-error-message', [
                'message' => $errors->first('open_graph_description')
                ])
        @endif
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('open_graph_images') ? 'has-error has-feedback' : '' }}
                {{ $errors->has('open_graph_images.*') ? 'has-error has-feedback' : '' }}">
        
                <label for="open_graph_images">Image</label>
            
                <input type="file" name="open_graph_images[][image]" multiple class="form-control">
            
                @if ($errors->has('open_graph_images'))
                    @include('common.form.input-error-message', [
                        'message' => $errors->first('open_graph_images')
                    ])
                @endif
            
                @if ($errors->has('open_graph_images.*'))
                    @foreach ($errors->get('open_graph_images.*') as $image)
                        @include('common.form.input-error-message', ['message' => $image[0]])
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for=""></label>
                <div class="form-control" style="border: none;">
                    <span>— OR —</span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <div class="radio" style="padding-top: 15px;">
                    <input type="checkbox" name="open_graph_use_featured_iamge" id="open_graph_use_featured_iamge" 
                            value="1"> Use Featured Image
                </div>
            </div>
        </div>
    </div>

    <br>

    <h4>Twitter</h4>

    <div class="form-group {{ $errors->has('twitter_card_type') ? 'has-error' : '' }}">
        <label for="twitter_card_type">Card Type</label>

        <div class="radio" style="margin-left: 20px;">
            <label for="twitter_card_type-summary">
                <input type="radio" value="summary"
                        name="twitter_card_type" id="twitter_card_type-summary" 
                        
                        {{ (old('twitter_card_type') == 'summary') ? 'checked' : 'checked' }} 
                        
                        {{ isset($post->social->twitter_card_type) ? 
                            (($post->social->twitter_card_type == 'summary') ? 'checked' : '') : '' }}> 

                Summary
            </label>
            <label for="twitter_card_type-large-image">
                <input type="radio" value="large image"
                        name="twitter_card_type" id="twitter_card_type-large-image" 
                        
                        {{ (old('twitter_card_type') == 'large image') ? 'checked' : '' }}
                        
                        {{ isset($post->social->twitter_card_type) ? 
                            (($post->social->twitter_card_type == 'large image') ? 'checked' : '') : '' }}> 
                
                Large Image
            </label>

            @if ($errors->has('twitter_card_type'))
                @include('components.form.input-error-message-no-feedback', [
                    'message' => $errors->first('twitter_card_type'
                )])
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('twitter_title') ? 'has-error has-feedback' : '' }}">
        <label for="twitter_title">Title</label>
    
        <input type="text" class="form-control" id="twitter_title" name="twitter_title"
            value="{{ old('twitter_title') ?: 
                    (isset($post->social->twitter_title) ? $post->social->twitter_title : '') }}">

        @if ($errors->has('twitter_title'))
            @include('common.form.input-error-message', [
                'message' => $errors->first('twitter_title')
            ])
        @endif
    </div>

    <div class="form-group {{ $errors->has('twitter_description') ? 'has-error has-feedback' : '' }}">
        <label for="twitter_description">Description</label>
    
        <textarea class="form-control" id="twitter_description" name="twitter_description"
                    rows="3">{{ old('twitter_description') ?: 
                                (isset($post->social->twitter_description) ? 
                                $post->social->twitter_description : '') }}</textarea>
    
        @if ($errors->has('twitter_description'))
            @include('common.form.input-error-message', [
                'message' => $errors->first('twitter_description')
                ])
        @endif
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('twitter_images') ? 'has-error has-feedback' : '' }}
                {{ $errors->has('twitter_images.*') ? 'has-error has-feedback' : '' }}">
        
                <label for="twitter_images">Image</label>
            
                <input type="file" name="twitter_images[][image]" multiple class="form-control">
            
                @if ($errors->has('twitter_images'))
                    @include('common.form.input-error-message', [
                        'message' => $errors->first('twitter_images')
                    ])
                @endif
            
                @if ($errors->has('twitter_images.*'))
                    @foreach ($errors->get('twitter_images.*') as $image)
                        @include('common.form.input-error-message', ['message' => $image[0]])
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for=""></label>
                <div class="form-control" style="border: none;">
                    <span>— OR —</span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <div class="radio" style="padding-top: 15px;">
                    <input type="checkbox" name="twitter_use_featured_iamge" id="twitter_use_featured_iamge" 
                            value="1"> Use Featured Image
                </div>
            </div>
        </div>
    </div>
</fieldset>
@component('common.buttons.submit')
    @slot('size', 'lg')
    @slot('text', 'Preview')
    @slot('target', '_blank')
    @slot('class', 'btn-block')
    @slot('value', 'preview')
@endcomponent

@component('common.buttons.submit')
    @slot('size', 'lg')
    @slot('color', 'warning')
    @slot('text', 'Save Draft')
    @slot('class', 'btn-block')
    @slot('value', 'draft')
@endcomponent

@component('common.buttons.submit')
    @slot('size', 'lg')
    @slot('color', 'primary')
    @slot('text', 'Publish')
    @slot('class', 'btn-block')
    @slot('value', 'publish')
@endcomponent

{{--  @if (isset($post)) 
    @if ($post->published_at)
        @component('common.buttons.submit')
            @slot('size', 'lg')
            @slot('color', 'warning')
            @slot('text', 'Save Draft')
            @slot('class', 'btn-block')
            @slot('value', 'draft')
        @endcomponent
    @else
        @component('common.buttons.submit')
            @slot('size', 'lg')
            @slot('color', 'primary')
            @slot('text', 'Publish')
            @slot('class', 'btn-block')
            @slot('value', 'publish')
        @endcomponent
    @endif
@else
    @component('common.buttons.submit')
        @slot('size', 'lg')
        @slot('text', 'Save Draft')
        @slot('class', 'btn-block')
        @slot('value', 'draft')
    @endcomponent

    @component('common.buttons.submit')
        @slot('size', 'lg')
        @slot('color', 'primary')
        @slot('text', 'Publish')
        @slot('class', 'btn-block')
        @slot('value', 'publish')
    @endcomponent
@endif  --}}
@extends('backend.layout.master')

@section('content-title', 'Post Detail')

@section('content-header')
    <span class="text-muted">Title: </span>

    <span id="post-title">{{ $post->title }}</span>
@endsection

@section('content-body')
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="text-center">
                        @if ($post->featured_image)
                            <img src="{{ asset('storage/' . $post->featured_image->path) }}" height="200px">
                        @else
                            <div class="text-center">
                                <i class="fa fa-picture-o fa-5x text-muted"></i>
                            </div>
                        @endif
                    </div>

                    <br>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <strong>Categories</strong>

                            <span class="pull-right">
                                {{ $post->category_list }}
                            </span>
                        </li>
                        <li class="list-group-item">
                            <strong>Tag</strong>

                            <span class="pull-right">
                                {{ $post->tag_list }}
                            </span>
                        </li>
                        <li class="list-group-item">
                            <strong>Creator</strong>

                            <span class="pull-right">
                                <a href="#">
                                    {{ $post->user->name }} 
                                </a>
                            </span>
                        </li>
                        <li class="list-group-item" style="height: 60px;">
                            <strong>Created At</strong>

                            <span class="pull-right">
                                {!! $post->created_at_formatted !!}
                            </span>
                        </li>
                        <li class="list-group-item" style="height: 60px; border-bottom: none;">
                            <strong>Published</strong>

                            <span class="pull-right text-right">
                                {!! $post->published_at_formatted !!}
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#post-content" data-toggle="tab">Content</a>
                    </li>
                    <li>
                        <a href="#post-seo" data-toggle="tab">SEO</a>
                    </li>
                    <li>
                        <a href="#post-social-seo" data-toggle="tab">Social SEO</a>
                    </li>
                </ul>

                <div class="tab-content">
                    @include('backend.post.detail.content')
                    @include('backend.post.detail.seo')
                    @include('backend.post.detail.social-seo')
                </div>
            </div>
        </div>
    </div>

    <div class="text-right">
        <form method="post" action="{{ route('post.update', $post) }}" style="display: inline;">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}

            @if ($post->published_at)
                @component('common.buttons.submit')
                    @slot('color', 'warning')
                    @slot('value', 'draft')
                    @slot('text', 'Set as Draft')
                @endcomponent
            @else
                @component('common.buttons.submit')
                    @slot('color', 'info')
                    @slot('value', 'publish')
                    @slot('text', 'Publish Now')
                @endcomponent
            @endif
        </form>

        @component('common.datalist.button-edit')
            @slot('size', 'md')
            @slot('text', 'Edit')
            @slot('route', route('post.edit', $post))
        @endcomponent
    </div>
@endsection


@push('header-scripts')
    <style>
        #post-title {
            font-size: 26px;
        }
        
        h4 { 
            border-bottom: 1px solid #ddd;
            color: #666;
            margin-bottom: 20px;
            padding-bottom: 10px;
            width: 400px;
        }
    </style>
@endpush
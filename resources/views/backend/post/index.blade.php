@extends('backend.layout.master')

@section('content-title', 'POST')

@section('content-header', 'Post List')

@section('content-body')

    <div style="margin-bottom: 60px;">
        @component('common.buttons.create-new')
            @slot('text', 'New Post')
            @slot('route', route('post.create'))
        @endcomponent
    </div>

    @if ($posts->isEmpty())
        @include('common.datalist.no-data')
    @else
        <table id="table1" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-center">Title</th>
                    <th class="text-center" style="width: 60px;"><i class="fa fa-heart"></i></th>
                    <th class="text-center">Creator</th>
                    <th class="text-center">Created At</th>
                    <th class="text-center">Published</th>
                    <th style="width: 180px;"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                    <tr>
                        <td class="text-center">
                            @if ($post->featured_image)
                                <img src="{{ asset('storage/' . $post->featured_image->path) }}" width="200px;">
                            @else
                                <div class="text-center">
                                    <i class="fa fa-picture-o fa-5x text-muted"></i>
                                </div>
                            @endif
                        </td>
                        <td>
                            <h5 style="font-size: 16px;">
                                <span class="text-muted">Title: </span>

                                <a href="{{ route('post.show', $post) }}">
                                    {{ $post->title }}
                                </a>
                            </h5>

                            <small class="text-muted">
                                Categories: {{ $post->category_list }}
                            </small> <br>

                            <small class="text-muted">
                                Tags: {{ $post->tag_list }}
                            </small>
                        </td>
                        <td>
                            @component('common.blog.love-count')
                                @slot('post', $post)
                                @slot('text_alignment', 'text-center')
                            @endcomponent
                        </td>
                        <td>
                            {{ $post->user->name }} <br>

                            <span class="fa fa-envelope-o"></span>
                            <a href="mailto:{{ $post->user->email }}">
                                {{ $post->user->email }}
                            </a>
                        </td>
                        <td class="text-center">
                            {!! $post->created_at_formatted !!}
                        </td>
                        <td class="text-center">
                            {!! $post->published_at_formatted !!}
                        </td>
                        <td class="text-right">
                            <form method="post" action="{{ route('post.update', $post) }}" style="display: inline;">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
    
                                @if ($post->published_at)
                                    @component('common.buttons.submit')
                                        @slot('size', 'sm')
                                        @slot('color', 'warning')
                                        @slot('value', 'draft')
                                        @slot('text', 'Set as Draft')
                                    @endcomponent
                                @else
                                    @component('common.buttons.submit')
                                        @slot('size', 'sm')
                                        @slot('color', 'info')
                                        @slot('value', 'publish')
                                        @slot('text', 'Publish Now')
                                    @endcomponent
                                @endif
                            </form>

                            @component('common.datalist.button-edit')
                                @slot('text', '')
                                @slot('route', route('post.edit', $post))
                            @endcomponent

                            @component('common.datalist.button-remove')
                                @slot('text', '')
                                @slot('route', route('post.destroy', $post))
                            @endcomponent
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="pull-right">
            {{ $posts->links() }}
        </div>
    @endif

    @each('backend.post.modal.love-count', $posts, 'post')
@endsection

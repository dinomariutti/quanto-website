<aside class="main-sidebar">
    <section class="sidebar">

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">NAVIGATION</li>

            @role('admin')
                <li>
                    <a href="{{ route('dashboard.index') }}">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
            @endrole

            @role('admin')
                <li class="treeview">
                    <a href="#post"><i class="fa fa-file-text-o"></i> <span>Blog</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('post.index') }}">
                                <i class="fa fa-circle-o"></i> Post
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('category.index') }}">
                                <i class="fa fa-circle-o"></i> Category
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('tag.index') }}">
                                <i class="fa fa-circle-o"></i> Tag
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('like.index') }}" class="hide">
                                <i class="fa fa-circle-o"></i> 
                                Like <i class="fa fa-heart-o"></i>
                            </a>
                        </li>
                    </ul>
                </li>
            @endrole

            @role('admin')
            <li>
                <a href="{{ route('user.index') }}">
                    <i class="fa fa-user"></i> <span>User</span>
                </a>
            </li>
            @endrole
        </ul>

    </section>
</aside>
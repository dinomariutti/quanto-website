@extends('layouts._default')

@push('page-meta')
<title>404 Error. The page you are looking for is not found - 123Quanto</title>
@endpush

@push('body-class')
<body id="error-page">
@endpush


@section('content')
<section id="hero-image is-in-view" class="hero-image" data-interchange="[{{ asset('assets/img/background-error-large.jpg') }}, xsmall], [{{ asset('assets/img/background-error-large.jpg') }}, small], [{{ asset('assets/img/background-error-large.jpg') }}, medium], [{{ asset('assets/img/background-error-large.jpg') }}, large]">
	<div class="grid-container">
    	<div class="grid-x grid-padding-x align-center-middle">
    		<div class="text-center">
	    		<p class="lead">The page you are looking for is not found</p>

	    		<h3>Download the 123Quanto app</h3>
	    		
	    		<ul class="download-badge">
					<li>
						<a><img src="{{ asset('assets/img/app_store_badge.svg') }}"  width="165" height="40" alt="Download 123Quanto app for iOS"></a>
					</li>
					
					<li>
						<a><img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android"></a>
					</li>
				</ul>
				
				<p><small>Attention Dealers: All Dealers are required to register via our website <a href="https://auction.123quanto.com/signin">here</a>.<br>All Dealer accounts must be approved by 123Quanto. You will be notified upon approval.</small></p>
	    	</div>
    	</div>
	</div>
</section>
@endsection
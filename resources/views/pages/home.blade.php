@extends('layouts._base')

@push('page-meta')
<title>Welcome to 123Quanto</title>
@endpush

@push('body-class')
<body id="home-page">
@endpush


@section('content')
<section id="hero-image" class="hero-image" data-interchange="[{{ asset('assets/img/background-hero-image-home-large.jpg') }}, xsmall], [{{ asset('assets/img/background-hero-image-home-large.jpg') }}, small], [{{ asset('assets/img/background-hero-image-home-large.jpg') }}, medium], [assets/img/background-hero-image-home-large.jpg') }}, large]">
	<div class="grid-container" id="magellan" data-magellan>
    	<div class="grid-x grid-padding-x align-middle">
	    	<div class="cell xsmall-12 large-6 text-center large-text-left xsmall-order-2 large-order-1">
		    	<h2 class="lead large-text-left" id="hero-image-text-1">BC’s fastest way to <br>sell your used car, <br>used truck or <br>used SUV</h2>

				<a class="cta" id="hero-image-cta" data-loc="#how-it-works-video">Watch the video</a>
	    	</div>
	    	
	    	<div class="cell xsmall-12 large-6 xsmall-order-1 large-order-2 phones">
		    	<div class="grid-x align-center-middle">
			    	<img src="{{ asset('assets/img/home-phone-2.png') }}" id="hero-image-phone-2" alt="Selling used car Vancouver" data-interchange="[{{ asset('assets/img/home-phone-2-small.png') }}, xsmall], [{{ asset('assets/img/home-phone-2-small.png') }}, small], [{{ asset('assets/img/home-phone-2.png') }}, medium], [{{ asset('assets/img/home-phone-2.png') }}, large]">
			    	
			    	<img src="{{ asset('assets/img/home-phone-1.png') }}" id="hero-image-phone-1" alt="Selling truck Vancouver" data-interchange="[{{ asset('assets/img/home-phone-1-small.png') }}, xsmall], [{{ asset('assets/img/home-phone-1-small.png') }}, small], [{{ asset('assets/img/home-phone-1.png') }}, medium], [{{ asset('assets/img/home-phone-1.png') }}, large]">
			    	
			    	<img src="{{ asset('assets/img/home-phone-3.png') }}" id="hero-image-phone-3" alt="Selling my vehicle Vancouver" data-interchange="[{{ asset('assets/img/home-phone-3-small.png') }}, xsmall], [{{ asset('assets/img/home-phone-3-small.png') }}, small], [{{ asset('assets/img/home-phone-3.png') }}, medium], [{{ asset('assets/img/home-phone-3.png') }}, large]" class="hide-for-small-only">
		    	</div>
	    	</div>
    	</div>
	</div>
</section>

<section id="countdown-container" class="countdown-container grid-container">
	<div class="grid-x align-center">
		<div class="cell xsmall-12 medium-12 large-10">
			<h1 class="text-center">Online Car auction in Vancouver</h1>
			
			<h2 class="text-center">Selling used car in Vancouver</h2>
		</div>
	</div>
	
	<div class="grid-x align-center">
		<div class="cell xsmall-12 medium-10 large-7 box">
			<div class="left grid-x align-center-middle">
				<p class="icon">
					<i class="fa fa-clock-o"></i>
				</p>
				
				<p>Daily<br>Auctions<br><span>2-3pm PST</span></p>
			</div>
			
			<div class="right grid-x align-middle">
				<p class="text-center medium-text-left">Sell your <br>car today</p>
				
				<p class="clock text-center medium-text-left" id="countdown-clock"></p>
				
				<p class="text-center medium-text-left">Until <br>Next Auction</p>
			</div>
		</div>
	</div>
</section>

<section id="how-it-works" class="how-it-works">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell large-8 text-center">
				<h3>How it works</h3>
				
				<img src="{{ asset('assets/img/how-it-works-large.png') }}" alt="How to sell your SUV Vancouver">
				
				<a class="cta" href="{{ url('tips') }}">Learn more</a>
				
				<p class="download-app">Download the app today</p>
				
				<ul class="download-badge">
					<li>
						<a href="https://itunes.apple.com/ca/app/123quanto/id1251855144?mt=8">
							<img src="{{ asset('assets/img/app_store_badge.svg') }}" width="165" height="40" alt="Download 123Quanto app for iOS">
						</a>
					</li>
					
					<li>
						<a href="https://play.google.com/store/apps/details?id=com.bilinedev.quanto&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
							<img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android">
						</a>
					</li>
				</ul>
				
				<p><small>Attention Dealers: All Dealers are required to register via our website <a href="https://auction.123quanto.com/signin">here</a>.<br>All Dealer accounts must be approved by 123Quanto. You will be notified upon approval.</small></p>
			</div>
		</div>
	</div>
</section>

<section id="things-to-know" class="things-to-know">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell large-8 large-offset-2 text-center">
				<h2 id="things-to-know-text">Things to know when selling your first car with 123Quanto</h2>
				
				<a class="cta" id="things-to-know-cta" href="{{ url('tips') }}">Click to learn</a>
			</div>
		</div>
	</div>
</section>

<section id="how-it-works-video" class="how-it-works-video" data-magellan-target="how-it-works-video">
	<div class="grid-container full" id="video">
		<div class="grid-x align-center">
			<div class="cell xsmall-12 large-8">
				<video id="preview-player" class="video-js vjs-fluid" controls data-setup={} preload="auto" poster="/assets/img/poster-how-it-works-video.jpg">
					<source src="{{ asset('/assets/video/123quanto-how-it-works.mp4') }}" type="video/mp4">
					<source src="{{ asset('/assets/video/123quanto-how-it-works.webm') }}" type="video/webm">
					<source src="{{ asset('/assets/video/123quanto-how-it-works.ogv') }}" type="video/ogg">
				</video>
			</div>
		</div>
	</div>
</section>

<section id="download-123quanto" class="download-123quanto" data-interchange="[assets/img/background-download-large.jpg, xsmall], [assets/img/background-download-large.jpg, small], [assets/img/background-download-large.jpg, medium], [assets/img/background-download-large.jpg, large]">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell text-center">
				<h2 id="download-123quanto-text-1">Download <img src="{{ asset('assets/img/logo-123quanto-white.svg') }}" width="200" height="32" alt="123Quanto logo"> today</h2>
				
				<p class="text-center" id="download-123quanto-text-2">Now available for  Greater Vancouver and the Fraser Valley</p>
				
                <div class="phones grid-x grid-padding-x align-center-middle" id="download-123quanto-phones">
                    <img src="{{ asset('assets/img/screen-ios.png') }}" alt="Public Auto Auction Vancouver">

                    <img src="{{ asset('assets/img/screen-android.png') }}" alt="Online auto auction Vancouver">
                </div>
                
				<ul class="download-badge" id="download-123quanto-download-badge">
					<li>
						<a href="https://itunes.apple.com/ca/app/123quanto/id1251855144?mt=8">
							<img src="{{ asset('assets/img/app_store_badge.svg') }}" width="165" height="40" alt="Download 123Quanto app for iOS">
						</a>
					</li>
					
					<li>
						<a href="https://play.google.com/store/apps/details?id=com.bilinedev.quanto&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
							<img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android">
						</a>
					</li>
				</ul>
                
                <p id="download-123quanto-text-3"><small>Attention Dealers: All Dealers are required to register via our website <a href="https://auction.123quanto.com/signin">here</a>.<br>All Dealer accounts must be approved by 123Quanto. You will be notified upon approval.</small></p>
                
                <p id="download-123quanto-text-4">Share</p>
                    
                <ul class="share" id="download-app-section-share">
                    <li>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url('/download-app')) }}" class="socmed-share">
                        	<i class="fa fa-facebook-square fa-2x"></i>
                        </a>
                    </li>

                    <li>
                        <a href="https://twitter.com/home?status=Download%20the%20123Quanto%20app%20today.%20{{ urlencode(url('/download-app')) }}" class="socmed-share">
                        	<i class="fa fa-twitter fa-2x"></i>
                        </a>
                    </li>
                </ul>
			</div>
		</div>
	</div>
</section>

<section id="blog" class="blog-list">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell large-10 large-offset-1 text-center">
				<h1 id="blog-text-1">The Open Road</h1>
				
				<p id="blog-text-2">Articles for sellers, dealers and auto enthusiasts</p>
			</div>
		</div>
	</div>
	
	<div class="grid-container">
		@foreach ($featured_posts as $post)
			@if ($loop->first)
				<div class="grid-x grid-padding-x align-center blog-items" id="blog-items-1">
					<div class="cell xsmall-12 medium-6 large-9 item large">
						@if ($post->featured_image)
							<img src="{{ asset(imageFly('storage/' . $post->featured_image->path, [870, 327])) }}" width="100%" alt="{{ $post->title }}">
						@endif

						<div class="container">
							<div class="box">
								<p class="title">
									<a href="{{ route('blog.article', $post) }}">
										{{ $post->title }}
									</a>
								</p>
								
								<p>{{ $post->excerpt }}</p>

								<footer>
									@include('common.blog.love', ['data' => $post])

									<p class="share">
										@component('common.buttons.share.facebook')
											@slot('data', $post)
										@endcomponent

										@component('common.buttons.share.twitter')
											@slot('post', $post)
										@endcomponent

										@component('common.buttons.share.linkedin')
											@slot('post', $post)
										@endcomponent
									</p>
								</footer>
							</div>
						</div>
					</div>
			@elseif ($loop->iteration == 2)
					<div class="cell xsmall-12 medium-6 large-3 item small">
						@if ($post->featured_image)
							<img src="{{ asset(imageFly('storage/' . $post->featured_image->path, [400, 150])) }}" width="100%" alt="{{ $post->title }}">
						@endif

						<p class="title">
							<a href="{{ route('blog.article', $post) }}">
								{{ $post->title }}
							</a>
						</p>

						<p>{{ $post->excerpt }}</p>

						<footer>
							@include('common.blog.love', ['data' => $post])
							
							<p class="share">
								@component('common.buttons.share.facebook')
									@slot('data', $post)
								@endcomponent

								@component('common.buttons.share.twitter')
									@slot('post', $post)
								@endcomponent

								@component('common.buttons.share.linkedin')
									@slot('post', $post)
								@endcomponent
							</p>
						</footer>
					</div>
				</div>
			@endif
		@endforeach

		@foreach ($remaining_posts->chunk(4) as $chunked_posts)
			<div data-equalizer="title">
				<div class="grid-x grid-padding-x xsmall-up-1 medium-up-2 large-up-4 blog-items" id="blog-items-2" data-equalizer="excerpt">
					@foreach ($chunked_posts as $post)
						<div class="cell item small">
							@if ($post->featured_image)
								<img src="{{ asset(imageFly('storage/' . $post->featured_image->path, [400, 150])) }}" width="100%" alt="{{ $post->title }}">
							@endif

							<p class="title" data-equalizer-watch="title">
								<a href="{{ route('blog.article', $post) }}">
									{{ $post->title }}
								</a>
							</p>
							
							<p data-equalizer-watch="excerpt">{{ $post->excerpt }}</p>
							
							<footer>
								@include('common.blog.love', ['data' => $post])

								<p class="share">
									@component('common.buttons.share.facebook')
										@slot('data', $post)
									@endcomponent

									@component('common.buttons.share.twitter')
										@slot('post', $post)
									@endcomponent

									@component('common.buttons.share.linkedin')
										@slot('post', $post)
									@endcomponent
								</p>
							</footer>
						</div>
					@endforeach
				</div>
			</div>
		@endforeach
	</div>
</section>
@endsection


@push('page-meta')
	@include('common.meta.facebook', ['post' => $post ?? null])
@endpush

@push('page-scripts')
	<script>window.twttr = (function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0],
		t = window.twttr || {};
	if (d.getElementById(id)) return t;
	js = d.createElement(s);
	js.id = id;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);

	t._e = [];
	t.ready = function(f) {
		t._e.push(f);
	};

	return t;
	}(document, "script", "twitter-wjs"));</script>
@endpush

@push('body-scripts')
	{{--  Load Facebook SDK for JavaScript  --}}
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1946661455653025&autoLogAppEvents=1';
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
@endpush

@push('page-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.2.8/video-js.min.css">
@endpush


@push('page-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone.min.js"></script>

<script>
    var player = videojs('preview-player', {
		fluid: true
	});

	$('#hero-image-cta').on('click', function() {
		var loc = $(this).attr('data-loc');

		$('#magellan').foundation('scrollToLoc', $(loc));
	});
	
	$('#quotes-carousel').slick({
		infinite: false,
		arrows: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		dots: true,
		responsive: [
			{
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
			{
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		],
		
	});
	
	
	// time to auction
	function time_to_auction() {
		var next_auction;
				   	
		var today = moment.tz("America/Los_Angeles");
		
		var auction_today_start = moment.tz("America/Los_Angeles");
		auction_today_start = auction_today_start.set('hour', 14);
		auction_today_start = auction_today_start.set('minute', 0);
		auction_today_start = auction_today_start.set('second', 0);
		
		var auction_today_end = moment.tz("America/Los_Angeles");
		auction_today_end = auction_today_end.set('hour', 14);
		auction_today_end = auction_today_end.set('minute', 59);
		auction_today_end = auction_today_end.set('second', 59);
		
		console.log(today.format("YYYY-MM-DD HH:mm:ss") + "   " + auction_today_start.format("YYYY-MM-DD HH:mm:ss") + "   " + auction_today_end.format("YYYY-MM-DD HH:mm:ss"));
		
		if (moment(today.format("YYYY-MM-DD HH:mm:ss")).isBefore(moment(auction_today_start.format("YYYY-MM-DD HH:mm:ss")))) {
			next_auction = auction_today_start;
			
		} else if(moment(today.format("YYYY-MM-DD HH:mm:ss")).isAfter(moment(auction_today_end.format("YYYY-MM-DD HH:mm:ss")))) {
			next_auction = auction_today_start.add(1, 'day');
		}
		
		return moment.tz(next_auction.format("YYYY-MM-DD HH:mm:ss"), "America/Los_Angeles");
	}
	
	// auction countdown timer
	moment.tz.add('America/Los_Angeles|PST PDT|80 70|0101|1Lzm0 1zb0 Op0');
	 
	var next_auction = time_to_auction();
	
	$('#countdown-clock').countdown(next_auction.toDate(), function(event) {
		$(this).html(event.strftime('%H<span>h</span>%M<span>m</span>%S<span>s</span>'));
	});
	
	//
	$('.blog-items').each(function()
	{
		var element = $(this).attr('id');
		
		var item_counter = 0;
		$(this).find('.item').each(function()
		{
			$(this).prop('style', 'transition-delay:'+ ((item_counter * 0.125) + 1)  + 's');
			item_counter++;
		});
		
		var controller = new ScrollMagic.Controller();
		
		var scene = new ScrollMagic.Scene({
			triggerElement: "#" + element,
			//triggerHook: "onEnter",
			duration: "100%"
			
		})
		//.addIndicators()
		.offset(-100)
		.on("enter", function(){
			$("#" + element).addClass('is-in-view');
		})
		.addTo(controller);
		
	});
	
	var quote_counter = 0;
	$('#quotes-carousel').find('.quote').each(function()
	{
		$(this).prop('style', 'transition-delay:'+ ((quote_counter * 0.125) + 0.5)  + 's');
		quote_counter++;
	});
	
	//
	var scene1_controller = new ScrollMagic.Controller();
	
	var scene1 = new ScrollMagic.Scene({
		triggerElement: "#hero-image",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#hero-image").addClass('is-in-view');
	})
	.addTo(scene1_controller);
	
	//
	var scene2_controller = new ScrollMagic.Controller();
	
	var scene2 = new ScrollMagic.Scene({
		triggerElement: "section#countdown-container",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.offset(-100)
	.on("enter", function(){
		$("section#countdown-container").addClass('is-in-view');
	})
	.addTo(scene2_controller);
	
	//
	var scene3_controller = new ScrollMagic.Controller();
	
	var scene3 = new ScrollMagic.Scene({
		triggerElement: "section#how-it-works",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#how-it-works").addClass('is-in-view');
	})
	.addTo(scene3_controller);
	
	//
	var scene4_controller = new ScrollMagic.Controller();
	
	var scene4 = new ScrollMagic.Scene({
		triggerElement: "section#things-to-know",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#things-to-know").addClass('is-in-view');
	})
	.addTo(scene4_controller);
	
	//
	var scene5_controller = new ScrollMagic.Controller();
	
	var scene5 = new ScrollMagic.Scene({
		triggerElement: "section#download-123quanto",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#download-123quanto").addClass('is-in-view');
	})
	.addTo(scene5_controller);
	
	//
	var scene6_controller = new ScrollMagic.Controller();
	
	var scene6 = new ScrollMagic.Scene({
		triggerElement: "section#blog",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.offset(-100)
	.on("enter", function(){
		$("section#blog").addClass('is-in-view');
	})
	.addTo(scene6_controller);
</script>
@endpush
@extends('layouts._base')

@push('page-meta')
<title>123Quanto Tips</title>
@endpush

@push('body-class')
<body id="tips-page">
@endpush

@section('content')
<div class="hero-image grid-x grid-padding-x" data-interchange="[assets/img/background-hero-image-home-large.jpg, xsmall], [assets/img/background-hero-image-home-large.jpg, small], [assets/img/background-hero-image-home-large.jpg, medium], [assets/img/background-hero-image-home-large.jpg, large]">
	<div class="cell text-center">
    	<h1>123Quanto Tips</h1>
	</div>
</div>

<section>
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell xsmall-12 medium-10 item tip clean-it-up" id="clean-it-up">
				<div class="icon">
					<img src="{{ asset('assets/img/icon-clean-it-up.png') }}" alt="Selling used car Vancouver">
				</div>
				
				<h3>1. Clean it up.</h3>
				
				<p>A market value sale on your pre-owned vehicle could net you thousands of dollars, so it doesn’t make much sense not to invest a few on getting your used vehicle thoroughly cleaned and detailed.</p> 

				<p>Make sure that the exterior surface of your used vehicle is well-polished and free of dust, debris and scratches. Clutter, dirt, and dust should be scrupulously removed from the interior. If necessary, take a vacuum to all flat surfaces, and get any leather fabrics properly conditioned and treated.</p>
				
				<p>The fact is, we buy with our eyes before anything else, and no matter how well your pre-owned vehicle runs, inadequate maintenance will deter any serious buyers. </p>
			</div>
			
			<div class="cell xsmall-12 medium-10 item tip take-picture" id="take-picture">
				<div class="icon">
					<img src="{{ asset('assets/img/icon-take-picture.png') }}" alt="Selling truck Vancouver">
				</div>
				
				<h3>2. Take pictures</h3>
				
				<p>Make sure all your effort is on display for prospective buyers. Take several pictures of your newly cleaned and detailed pre-owned vehicle. If your phone camera isn’t doing the beauty of your used car justice, then invest in a good digital camera to take a high-quality photo that does. </p>
			</div>
			
			<div class="cell xsmall-12 medium-10 item tip history" id="history">
				<div class="icon">
					<img src="{{ asset('assets/img/icon-history.png') }}" alt="Selling my vehicle Vancouver">
				</div>
				
				<h3>3. Know Your History</h3>
				
				<p>Buyers are making a substantial investment in your pre-owned vehicle that means they need to be fully satisfied about what they’re getting before they’ll commit to a purchase.</p>
 
				<p>Make sure to have all relevant service records on hand, including details of repairs, maintenance work, servicing, 
mechanical work under warranty, possible information on recalls and ownership history if you’re not the first owner of the used vehicle.</p>
			</div>
			
			<div class="cell xsmall-12 medium-10 item tip get-it-inspected" id="get-it-inspected">
				<div class="icon">
					<img src="{{ asset('assets/img/icon-get-it-inspected.png') }}" alt="How to sell you SUV Vancouver">
				</div>
				
				<h3>4. Get it Inspected</h3>
				
				<p>To prevent any liability down the road, you should have your used car, used truck or used SUV  inspected by a licensed mechanic before putting it up for sale. Amongst other features, the mechanic will test your used car’s tired, brakes, lights, 
airbags and other mechanisms to ensure that the pre-owned vehicle is 100% safe to drive. They’ll check that the vehicle’s 
emissions are not above standard levels in your province or state. </p>
 
				<p>Once you’re verified on all fronts, you’ll receive a certification for your pre-owned vehicle which will help you when it comes time to sell.</p>
			</div>
			
			<div class="cell xsmall-12 medium-10 item tip competitive-price" id="competitive-price">
				<div class="icon">
					<img src="{{ asset('assets/img/icon-competitive-price.png') }}" alt="Public Auto Auction Vancouver">
				</div>

				<h3>5. Set a Competitive Price</h3>
				
				<p>It’s marketing 101. Once you’ve estimated a likely selling price for your used vehicle, make sure to set your asking price at a higher amount than you’re willing to accept so that the buyer has room for negotiations. </p>
			</div>
			
			<div class="cell xsmall-12 medium-10 item tip meeting-in-person" id="meeting-in-person">
				<div class="icon">
					<img src="{{ asset('assets/img/icon-meeting-in-person.png') }}" alt="Online auto auction Vancouver">
				</div>
				
				<h3>6. Don’t Accept any Sales Without an In-Person Meeting</h3>
				
				<p>As the old adage goes, if it’s too good to be true then it probably isn’t. Any buyer who’s willing to buy your used vehicle without even inspecting it first is probably looking to scam you.</p>
			</div>
			
			<div class="cell xsmall-12 medium-10 item tip no-cheques-money-orders" id="no-cheques-money-orders">
				<div class="icon">
					<img src="{{ asset('assets/img/icon-no-cheques-money-orders.png') }}" alt="Tips for selling my used suv Vancouver">
				</div>

				<h3>7. Don’t Accept Cheques or Money Orders</h3>
				
				<p>Without making sure the payment goes through first at least. It’s extremely common for scammers on Craigslist, Kijiji and other used car websites to use forged cheques to secure a sale.</p>
			</div>
			
			<div class="cell xsmall-12 medium-10 item tip overpayments" id="overpayments">
				<div class="icon">
					<img src="{{ asset('assets/img/icon-overpayments.png') }}" alt="Trade in my truck Vancouver">
				</div>
				
				<h3>8. Look into Overpayments</h3>
				
				<p>If someone is willing to offer you far more than the used car is generally worth, then you need to ask yourself why. Is the buyer promising you money from a third party that you don’t know? Are you being asked to accept payment in instalments? 
Be on the lookout for schemes that require complicated arrangements.</p>
			</div>
			
			<div class="cell xsmall-12 medium-10 item tip paperwork" id="paperwork">
				<div class="icon">
					<img src="{{ asset('assets/img/icon-paperwork.png') }}" alt="Car for Sale Vancouver">
				</div>
				
				<h3>9. Make Sure the Buyer has Paperwork</h3>
				
				<p>This includes proof of insurance, sales tax and registration money and driver’s license.</p>
			</div>
		</div>
	</div>
	
	<div class="background-green tip" id="plate">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell xsmall-12 item plate text-center">
					<p><strong>And Finally</strong></p>
					
					<div class="icon">
						<img src="{{ asset('assets/img/icon-plate.png') }}" alt="Truck auction Vancouver">
					</div>
					
					<p>Remove your plates as the ownership transfer of your used vehicle is completed at your local insurance provider.</p>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('page-scripts')
<script>
	$('.tip').each(function()
	{
		var element = $(this).attr('id');
		console.log(element);
		var controller = new ScrollMagic.Controller();
		
		var scene = new ScrollMagic.Scene({
				triggerElement: "#" + element,
				triggerHook: "onEnter",
				offset: 250,
				duration: "100%"
				
			})
			//.addIndicators()
			.on("enter", function(){
				$("#" + element).addClass('is-active');
			})
			.addTo(controller);
	});
</script>
@endpush
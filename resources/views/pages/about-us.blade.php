@extends('layouts._base')

@push('page-meta')
<title>About 123Quanto</title>
@endpush

@push('body-class')
<body id="about-us-page">
@endpush

@section('content')
<section id="hero-image" class="hero-image" data-interchange="[{{ asset('assets/img/background-hero-image-about-us-large.jpg') }}, xsmall], [{{ asset('assets/img/background-hero-image-about-us-large.jpg') }}, small], [{{ asset('assets/img/background-hero-image-about-us-large.jpg') }}, medium], [{{ asset('assets/img/background-hero-image-about-us-large.jpg') }}, large]">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell xsmall-12 large-6">
                <div class="grid-x align-middle">
                    <div class="cell">
                        <h1 class="text-center" id="hero-image-text-1">Welcome to 123Quanto</h1>

                        <p id="hero-image-text-2">Whether you’re an auto consumer or an auto dealer, we’ve all bought and sold used vehicles before and we all know about the hassles and limited options that make it difficult to do so. 123Quanto is an online auto auction created by a group of car enthusiasts and auto dealers to connect auto consumers with auto dealers at scale. 123Quanto is here to provide another option to find, buy and sell used cars, used trucks and used SUV’s in real-time.</p>
                    </div>
                </div>
			</div>
        
            <div class="cell xsmall-12 large-6 text-center">
                <div class="grid-x align-middle">
                    <div class="cell">
                        <h2 id="hero-image-text-3">Download 123Quanto<br>and get started</h2>

                        <ul class="download-badge" id="download-123quanto-download-badge">
                            <li>
                                <a href="https://itunes.apple.com/ca/app/123quanto/id1251855144?mt=8">
                                	<img src="{{ asset('assets/img/app_store_badge.svg') }}" width="165" height="40" alt="Download 123Quanto app for iOS">
                                </a>
                            </li>

                            <li>
                                <a href="https://play.google.com/store/apps/details?id=com.bilinedev.quanto&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
                                	<img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android">
                                </a>
                            </li>
                        </ul>
                        
                        <p id="hero-image-text-4"><small>Attention Dealers: All Dealers are required to register via our website <a href="https://auction.123quanto.com/signin">here</a>.<br>All Dealer accounts must be approved by 123Quanto. You will be notified upon approval.</small></p>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>

<section id="seller" class="seller">
	<div class="grid-container">
    	<div class="grid-x grid-padding-x">
	    	<div class="cell large-12">
		    	<h2 class="text-center">Sellers</h2>
	    	</div>
	    	
	    	<div class="grid-x grid-padding-x">
		    	<div class="cell xsmall-12 medium-6">
					<p>As consumers, we have very limited options when looking to sell our used vehicles.  We can attempt to sell our used vehicles privately but this can be a painful process managing showings, test drives and buyers that simply waste your time.  The private sale takes time and is not for everyone but will ultimately provide the most money for your used vehicle, which is great if you’ve got the patience and your willing to put up with the hassle.</p>
				</div>
				
				<div class="cell xsmall-12 medium-6">
			    	<p>Another option is to trade-in your used vehicle to a dealership.  When you trade-in your vehicle to a dealer you’re typically only talking to a handful of dealers at a time, you never really know if there are other interested dealers or if your getting the best deal.  When you trade in a vehicle to a dealer you receive tax savings on your new vehicle which almost makes up for the difference of selling privately.</p>
		    	</div>
	    	</div>
    	</div>
        
        <div class="grid-x grid-padding-x">
            <div class="cell xsmall-12 text-center">
                <a class="cta" href="{{ url('sellers') }}">Go to Sellers page</a>
            </div>
        </div>
	</div>

	<div class="feedback">
		<div class="grid-container">
	    	<div class="grid-x grid-padding-x">
		    	<div class="cell large-6">
			    	<h3 class="text-center large-text-left">We appreciate your valuable feedback</h3>
		    	</div>
		    	
		    	<div class="cell large-6">
			    	<p>We are committed to providing the best user experience possible and welcome your feedback.</p>
	
					<p>Help us improve our service. Please <a href="{{ url('contact-us') }}">contact us</a> with your suggestions.</p>
		    	</div>
	    	</div>
		</div>
	</div>
</section>

<section id="dealer" class="dealer">
	<div class="background">
    	<div class="grid-container">
	    	<div class="grid-x grid-padding-x">
		    	<div class="cell xsmall-12 large-6">
			    	<h2 class="text-center large-text-left">Dealers</h2>
			    	
			    	<p>We all know that the automotive industry is a very competitive landscape for dealers so they’re always interested in additional ways to find good used vehicles.  Like consumers, dealers have limited options when looking for used vehicles, typically they rely on wholesale auctions and customer trade-ins to fill their used inventory.  Finding good local used vehicles has always been a struggle for dealers and they’re willing to pay top dollar for the right used car, used truck or used SUV that compliments their lot.</p>
                    
                    <div class="text-center">
                        <a class="cta" href="{{ url('dealers') }}">Go to Dealers page</a>
                    </div>
		    	</div>
	    	</div>
    	</div>
	</div>
	
	<div id="dealer-feedback" class="feedback">
		<div class="grid-container">
	    	<div class="grid-x">
		    	<div class="cell large-10 large-offset-1 text-center">
			    	<p>123Quanto offers another option for auto consumers to sell their used car, used truck or used SUV, while providing auto dealers an alternate way to find and buy good local used vehicles.</p>
					
					<p>Dealers are ready to buy your used vehicle today.</p>
		    	</div>
	    	</div>
		</div>
	</div>
</section>
@endsection

@push('page-scripts')
<script>
	//
	var scene1_controller = new ScrollMagic.Controller();
	
	var scene1 = new ScrollMagic.Scene({
		triggerElement: "section#hero-image",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#hero-image").addClass('is-in-view');
	})
	.addTo(scene1_controller);
	
	//
	var scene2_controller = new ScrollMagic.Controller();
	
	var scene2 = new ScrollMagic.Scene({
		triggerElement: "section#seller",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#seller").addClass('is-in-view');
	})
	.addTo(scene2_controller);
	
	//
	var scene4_controller = new ScrollMagic.Controller();
	
	var scene4 = new ScrollMagic.Scene({
		triggerElement: "section#dealer",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#dealer").addClass('is-in-view');
	})
	.addTo(scene4_controller);
	
	//
	var scene6_controller = new ScrollMagic.Controller();
	
	var scene6 = new ScrollMagic.Scene({
		triggerElement: "section#download-123quanto",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#download-123quanto").addClass('is-in-view');
	})
	.addTo(scene6_controller);
</script>
@endpush
@extends('layouts._base')

@push('page-meta')
<title>How it Works - 123Quanto</title>
@endpush

@push('body-class')
<body id="how-it-works-page">
@endpush

@section('content')
<section id="hero-image" class="hero-image">
	<div class="grid-container">
    	<div class="grid-x grid-padding-x">
			<div class="cell xsmall-6 xsmall-offset-3 medium-4 medium-offset-4 large-4 large-offset-0 xsmall-order-2 large-order-1">   	
		    	<img id="hero-image-phone-1" src="{{ asset('assets/img/how-it-works-iphone.png') }}" alt="Tips for selling my used SUV Vancouver">
	    	</div>
	    	
	    	<div class="cell xsmall-12 medium-12 medium-offset-0 large-6 large-offset-2 xsmall-order-1 large-order-2 text-center medium-text-left">
	    		<div class="grid-x align-middle">
		    		<h1 class="text-center large-text-left" id="hero-image-text">Are you tired of trying to sell your used car, used truck or used SUV privately?<br>Wouldn’t it be great to have dealers compete for your used vehicle?</h1>
		    	</div>
	    	</div>
    	</div>
	</div>

	<div class="about-123quanto-part-1">
		<header>
	    	<div class="grid-container">
		    	<div class="grid-x grid-padding-x">
			    	<div class="cell large-12 text-center">
				    	<p>123Quanto is a real-time online auto auction that makes it fast and easy to get the highest value foryour used car, used truck or used SUV without leaving the comfort of your home.  123Quanto removes all of the hassles of trying to sell your vehicle privately, avoid inconvenient showings, test drives, and buyers wasting your valuable time! </p>
			    	</div>
		    	</div>
	    	</div>
		</header>
	</div>

	<div id="about-123quanto-part-2" class="about-123quanto-part-2">
		<div class="grid-container">
		    <div class="grid-x grid-padding-x">
			    <div class="cell large-12">
				    <div class="grid-x">
					    <div class="cell xsmall-12 medium-6">
						    <p>123Quanto is FREE to list, we get your vehicle in front of hundreds of local dealers who want to buy your vehicle today.</p>
					    </div>
					    
					    <div class="cell xsmall-12 medium-6">
						    <p>Need a vehicle appraisal?  List today and find out what dealers are willing to pay for your used car, used truck or used SUV.</p>
					    </div>
				    </div>
				    
				    <hr>
			    </div>
		    </div>
		
	    	<div class="grid-x grid-padding-x">
			    <div class="cell large-12 text-center">
				    <p>Start by simply scanning your VIN* (found in the driver side door jamb), then snap some photos, edit your details and allow local dealers to view, bid, and purchase your used vehicle.  Our network of dealers is standing by and ready to buy your used car, used truck or used SUV today.</p>
				    
				    <p><small>*Vehicles model 1981 and newer have a VIN or Vehicle Identification Number records the vehicles history and is made up of 17 characters (letters and numbers).  Before that, the VIN length and format varied among vehicles.  The VIN produces the vehicles information including the manufacturer, the model, the year and where it was built.</small></p>
			    </div>
	    	</div>
		</div>
	</div>
</section>	

<section id="download-123quanto" class="download-123quanto">
	<div class="grid-container">
		<div class="grid-x padding-x">
			<div class="cell text-center">
				<h2 id="download-123quanto-text-1">Download 123Quanto and get started</h2>
				
				<p id="download-123quanto-text-2">Serving the Lower Mainland and Fraser Valley</p>
				
				<ul class="download-badge" id="download-123quanto-download-badge">
					<li>
						<a href="https://itunes.apple.com/ca/app/123quanto/id1251855144?mt=8">
							<img src="{{ asset('assets/img/app_store_badge.svg') }}" width="165" height="40" alt="Download 123Quanto app for iOS">
						</a>
					</li>
					
					<li>
						<a href="https://play.google.com/store/apps/details?id=com.bilinedev.quanto&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
							<img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android">
						</a>
					</li>
				</ul>
				
				<p id="download-123quanto-text-3"><small>Attention Dealers: All Dealers are required to register via our website <a href="https://auction.123quanto.com/signin">here</a>.<br>All Dealer accounts must be approved by 123Quanto. You will be notified upon approval.</small></p>
				
                <p id="download-123quanto-text-4">Share</p>

                <ul class="share" id="download-app-section-share">
                    <li>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url('/download-app')) }}" class="socmed-share">
                        	<i class="fa fa-facebook-square fa-2x"></i>
                        </a>
                    </li>

                    <li>
                        <a href="https://twitter.com/home?status=Download%20the%20123Quanto%20app%20today.%20{{ urlencode(url('/download-app')) }}" class="socmed-share">
                        	<i class="fa fa-twitter fa-2x"></i>
                        </a>
                    </li>
                </ul>
			</div>
		</div>
	</div>
</section>

<div class="filler" id="how-to-use-app-scene1">
	<section class="scene-1" id="how-to-use-app">
		<div class="grid-container">
			<div class="grid-x align-center-middle">
				<div class="text left">
					<h2 class="text-center medium-text-left">Scan, <br>Snap, <br>Auction</h2>
					
					<p class="text-center medium-text-left">Easy as 123Quanto</p>
				</div>
				
				<div id="phone-base">
					<img src="{{ asset('assets/img/phone-base.png') }}" alt="Trade in my truck Vancouver">
					
					<img src="{{ asset('assets/img/screen-1.png') }}" alt="Car for Sale Vancouver" class="screens screen-1">
					
					<img src="{{ asset('assets/img/screen-2.png') }}" alt="Truck auction Vancouver" class="screens screen-2">
					
					<img src="{{ asset('assets/img/screen-3.png') }}" alt="Vehicle for sale Vancouver" class="screens screen-3">
					
					<img src="{{ asset('assets/img/screen-4.png') }}" alt="What is my car worth Vancouver" class="screens screen-4">
				</div>
				
				<div class="text right text-center medium-text-right">
					<div class="steps step-1">
						<p>1. Scan</p>
					</div>
					
					<div class="steps step-2">
						<p>2. Snap</p>
					</div>
					
					<div class="steps step-3">
						<p>3. Auction</p>
					</div>
					
					<div class="steps step-4">
						<p>Q. Profit</p>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<div class="filler" id="how-to-use-app-scene2"></div>

<div class="filler" id="how-to-use-app-scene3"></div>

<div class="filler" id="how-to-use-app-scene4"></div>

<div class="filler end"></div>

<section id="how-it-works-video" class="how-it-works-video" data-magellan-target="how-it-works-video">
	<div class="grid-container full" id="video">
		<div class="grid-x align-center">
			<div class="cell xsmall-12 large-8">
				<video id="preview-player" class="video-js vjs-fluid" controls data-setup={} preload="auto" poster="{{ asset('assets/img/poster-how-it-works-video.jpg') }}">
					<source src="{{ asset('/assets/video/123quanto-how-it-works.mp4') }}" type="video/mp4">
					<source src="{{ asset('/assets/video/123quanto-how-it-works.webm') }}" type="video/webm">
					<source src="{{ asset('/assets/video/123quanto-how-it-works.ogv') }}" type="video/ogg">
				</video>
			</div>
		</div>
	</div>
</section>

<section id="how-it-works" class="how-it-works">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell large-8 text-center">
				<h2>As easy as 1, 2, 3</h2>
				
				<img src="{{ asset('assets/img/how-it-works-large.png') }}" alt="SUV appraisal Vancouver">
				
				<a class="cta">Learn more</a>
				
				<p class="download-app">Download the app today</p>
				
				<ul class="download-badge">
					<li>
						<a href="https://itunes.apple.com/ca/app/123quanto/id1251855144?mt=8">
							<img src="{{ asset('assets/img/app_store_badge.svg') }}" width="165" height="40" alt="Download 123Quanto app for iOS">
						</a>
					</li>
					
					<li>
						<a href="https://play.google.com/store/apps/details?id=com.bilinedev.quanto&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
							<img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android">
						</a>
					</li>
				</ul>
				
				<p><small>Attention Dealers: All Dealers are required to register via our website <a href="https://auction.123quanto.com/signin">here</a>.<br>All Dealer accounts must be approved by 123Quanto. You will be notified upon approval.</small></p>
			</div>
		</div>
	</div>
</section>

<section id="feedback" class="feedback">
	<div class="grid-container">
    	<div class="grid-x">
	    	<div class="cell large-12">
		    	<div class="grid-x grid-padding-x">
			    	<div class="cell large-6">
				    	<h2 class="text-center large-text-left">We strive for excellence</h2>
			    	</div>
			    	
			    	<div class="cell large-6">
				    	<p>We are committed to providing the best user experience possible and welcome your feedback.</p>

						<p>Help us improve our service. Please <a href="{{ url('contact-us') }}">contact us</a> with your suggestions.</p>
			    	</div>
		    	</div>
	    	</div>
    	</div>
	</div>
</section>
@endsection

@push('page-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.2.8/video-js.min.css">
@endpush

@push('page-scripts')
<script>
	var player = videojs('preview-player', {
		fluid: true
	});

	//=
	var scene1_controller = new ScrollMagic.Controller();
	
	var scene1 = new ScrollMagic.Scene({
		triggerElement: "section#hero-image",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#hero-image").addClass('is-in-view');
	})
	.addTo(scene1_controller);
	
	//
	var scene3_controller = new ScrollMagic.Controller();
	
	var scene3 = new ScrollMagic.Scene({
		triggerElement: "#about-123quanto-part-2",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("#about-123quanto-part-2").addClass('is-in-view');
	})
	.addTo(scene3_controller);
	
	//
	var scene4_controller = new ScrollMagic.Controller();
	
	var scene4 = new ScrollMagic.Scene({
		triggerElement: "section#download-123quanto",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#download-123quanto").addClass('is-in-view');
	})
	.addTo(scene4_controller);
	
	//
	var scene5_controller = new ScrollMagic.Controller();
	
	var scene5 = new ScrollMagic.Scene({
		triggerElement: "section#how-it-works",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#how-it-works").addClass('is-in-view');
	})
	.addTo(scene5_controller);
	
	//
	var scene6_controller = new ScrollMagic.Controller();
	
	var scene6 = new ScrollMagic.Scene({
		triggerElement: "section#feedback",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#feedback").addClass('is-in-view');
	})
	.addTo(scene6_controller);
	
	
	//
	var how_to_use_app_controller_1 = new ScrollMagic.Controller();
	var how_to_use_app_controller_2 = new ScrollMagic.Controller();
	var how_to_use_app_controller_3 = new ScrollMagic.Controller();
	var how_to_use_app_controller_4 = new ScrollMagic.Controller();
	
	var how_to_use_app_scene_1 = new ScrollMagic.Scene({
		triggerElement: "#how-to-use-app-scene1",
		triggerHook: "onLeave",
		duration: "200%"
		
	})
	.on("enter", function(){
		$("#how-to-use-app").removeClass();
		
		$("#how-to-use-app").addClass('scene-1');
	})
	.on("leave", function(){
		
	})
	.setPin("#how-to-use-app")
	.addTo(how_to_use_app_controller_1);
				
	var how_to_use_app_scene_2 = new ScrollMagic.Scene({
		triggerElement: "#how-to-use-app-scene2",
		triggerHook: "onLeave",
		duration: "200%"
		
	})
	.on("enter", function(){
		$("#how-to-use-app").removeClass();
		
		$("#how-to-use-app").addClass('scene-2');
	})
	.on("leave", function(){
		
	})
	.setPin("#how-to-use-app")
				.addTo(how_to_use_app_controller_2);
				
	var how_to_use_app_scene_3 = new ScrollMagic.Scene({
		triggerElement: "#how-to-use-app-scene3",
		triggerHook: "onLeave",
		duration: "200%"
		
	})
	.on("enter", function(){
		$("#how-to-use-app").removeClass();
		
		$("#how-to-use-app").addClass('scene-3');
	})
	.on("leave", function(){
		
	})
	.setPin("#how-to-use-app")
	.addTo(how_to_use_app_controller_3);
				
	var how_to_use_app_scene_4 = new ScrollMagic.Scene({
		triggerElement: "#how-to-use-app-scene4",
		triggerHook: "onLeave",
		duration: "200%"
		
	})
	.on("enter", function(){
		$("#how-to-use-app").removeClass();
		
		$("#how-to-use-app").addClass('scene-4');
	})
	.on("leave", function(){
		
	})
	.setPin("#how-to-use-app")
	.addTo(how_to_use_app_controller_4);
</script>
@endpush
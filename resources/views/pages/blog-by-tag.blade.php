@extends('layouts._blog')

@push('page-meta')
	<title>The Open Road - 123Quanto Blog</title>
@endpush

@push('body-class')
	<body id="blog-page">
@endpush

@section('content')
	<section class="hero-image grid-x" data-interchange="[assets/img/background-hero-image-home-large.jpg, xsmall], [assets/img/background-hero-image-home-large.jpg, small], [assets/img/background-hero-image-home-large.jpg, medium], [assets/img/background-hero-image-home-large.jpg, large]">
		<div class="cell text-center">
			<h1>The Open Road</h1>

			<p>Articles for sellers, dealers and auto enthusiasts</p>
		</div>
	</section>

	<section class="blog-list">
		<div class="grid-container">
			<h2 style="font-size: 24px;">Posts by Tag: {{ $tag->name }}</h2>
			<div class="grid-x grid-padding-x">
				<div class="cell xsmall-12 large-9">
					
					@if ($posts->isEmpty())
						@component('common.datalist.no-data')
							@slot('text', 'No article for this tag')
							@slot('border_style', 'margin-top: 40px;')
							@slot('text_style', 'padding-top: 15px;')
						@endcomponent
					@else
						@foreach ($posts->chunk(3) as $chunked_posts)
							<div data-equalizer="excerpt">
								<div class="grid-x grid-padding-x xsmall-up-1 medium-up-2 large-up-3 blog-items" 
									id="blog-items-{{ ($loop->iteration + 1) }}" data-equalizer="title">
									@foreach ($chunked_posts as $post)
										<div class="cell item small">
											@if ($post->featured_image)
												<img src="{{ asset(imageFly('storage/' . $post->featured_image->path, [400, 150])) }}" width="100%" alt="{{ $post->title }}">
											@endif

											<p class="title" data-equalizer-watch="title">
												<a href="{{ route('blog.article', $post) }}">
													{{ $post->title }}
												</a>
											</p>
											
											<p data-equalizer-watch="excerpt">{{ $post->excerpt }}</p>
											
											<footer>
												<p><i class="fa fa-heart fa-lg"></i> 2000</p>

												<p class="share">
													@component('common.buttons.share.facebook')
														@slot('data', $post)
													@endcomponent

													@component('common.buttons.share.twitter')
														@slot('post', $post)
													@endcomponent

													@component('common.buttons.share.linkedin')
														@slot('post', $post)
													@endcomponent
												</p>
											</footer>
										</div>
									@endforeach
								</div>
							</div>
						@endforeach
					@endif
					
					<div class="grid-x grid-padding-x">
						<div class="cell text-center">
							{{ $posts->links('common.pagination.blog') }}
						</div>
					</div>
				</div>
				
				<aside class="blog cell large-3 hide-for-xsmall-only hide-for-small-only hide-for-medium-only">
					@include('layouts.blog-aside', ['tags' => $tags, 'categories' => $categories])
				</aside>
			</div>
		</div>
	</section>
@endsection


@push('page-meta')
	@include('common.meta.facebook', ['post' => $post ?? null])
@endpush

@push('body-scripts')
	{{--  Load Facebook SDK for JavaScript  --}}
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1946661455653025&autoLogAppEvents=1';
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
@endpush

@push('page-scripts')
	<script>window.twttr = (function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0],
		t = window.twttr || {};
	if (d.getElementById(id)) return t;
	js = d.createElement(s);
	js.id = id;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);

	t._e = [];
	t.ready = function(f) {
		t._e.push(f);
	};

	return t;
	}(document, "script", "twitter-wjs"));</script>
@endpush

@push('page-scripts')
<script>
var counter = 0;

$('.blog-items').each(function()
{
	var element = $(this).attr('id');
	
	var item_counter = 0;
	$(this).find('.item').each(function()
	{
		$(this).prop('style', 'transition-delay:'+ (item_counter * 0.125)  + 's');
		item_counter++;
	});
	
	var controller = new ScrollMagic.Controller();
	
	var scene = new ScrollMagic.Scene({
		triggerElement: "#" + element,
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("#" + element).addClass('is-in-view');
	})
	.addTo(controller);
	
	counter++;
	
});
</script>
@endpush
@extends('layouts._base')

@push('page-meta')
<title>Download 123Quanto App Today!</title>
@endpush

@push('body-class')
<body id="download-app-page">
@endpush

@section('content')
<section id="download-app" class="download-app-section" data-interchange="[{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, xsmall], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, small], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, medium], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, large]">
	<div class="grid-container">
	    	<div class="grid-x grid-padding-x">
		    	<div class="cell xsmall-8 xsmall-offset-2 large-4 large-offset-0 xsmall-order-2 large-order-1 text-center">
					<img src="{{ asset('assets/img/download-app-hand.png') }}" id="hand" alt="Truck auction Vancouver" data-interchange="[{{ asset('assets/img/download-app-hand-small.png') }}, xsmall], [{{ asset('assets/img/download-app-hand-small.png') }}, small], [{{ asset('assets/img/download-app-hand-small.png') }}, medium], [{{ asset('assets/img/download-app-hand.png') }}, large]" class="hand">
		    	</div>
		    	
		    	<div class="cell xsmall-12 xsmall-offset-0 large-6 large-offset-2 xsmall-order-1 large-order-2">
			    	<div class="grid-x padding-x align-middle">
				    	<div class="cell text-center">
					    	<p class="lead" id="download-app-text-1">Download the app now!</p>
					    	
					    	<p id="download-app-text-2">Serving Greater Vancouver and the Fraser Valley</p>
					    	
					    	<ul class="download-badge" id="download-123quanto-download-badge">
                                <li>
                                    <a hhttps://itunes.apple.com/ca/app/123quanto/id1251855144?mt=8>
                                    	<img src="{{ asset('assets/img/app_store_badge.svg') }}" width="165" height="40" alt="Download 123Quanto app for iOS">
                                    </a>
                                </li>

                                <li>
                                    <a href="https://play.google.com/store/apps/details?id=com.bilinedev.quanto&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
                                    	<img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android">
                                    </a>
                                </li>
                            </ul>
							
							<div class="text-center">
                                <p id="download-app-text-3">Share</p>
                                
								<ul class="share" id="download-app-section-share">
									<li>
				                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url('/download-app')) }}" rel="share">
				                        	<i class="fa fa-facebook-square fa-2x"></i>
				                        </a>
				                    </li>

				                    <li>
				                        <a href="https://twitter.com/home?status=Download%20the%20123Quanto%20app%20today.%20{{ urlencode(url('/download-app')) }}" rel="share">
				                        	<i class="fa fa-twitter fa-2x"></i>
				                        </a>
				                    </li>
								</ul>
							</div>
				    	</div>
			    	</div>
		    	</div>
	    	</div>
		</div>
	</div>
</section>
@endsection

@push('page-scripts')
<script>
	//
	var scene5_controller = new ScrollMagic.Controller();
	
	var scene5 = new ScrollMagic.Scene({
		triggerElement: "section#download-app",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#download-app").addClass('is-in-view');
	})
	.addTo(scene5_controller);
</script>
@endpush
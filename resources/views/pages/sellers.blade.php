@extends('layouts._base')

@push('page-meta')
<title>For Sellers - 123Quanto</title>
@endpush

@push('body-class')
<body id="sellers-page">
@endpush

@section('content')
<section id="seller" class="seller">
	<div class="background">
    	<div class="grid-container">
	    	<div class="grid-x grid-padding-x">
		    	<div class="cell xsmall-12 large-6 text-center">
			    	<h1 id="seller-text-1">For Sellers</h1>
			    	
			    	<p id="seller-text-2">Have you fallen out of love with used car, used truck or used SUV? Are you looking to upgrade to a newer model? Or do you simply need to make some extra room for a growing family? Whatever your reasons are, the decision to sell your pre-owned vehicle isn’t an easy one to make. Disregarding any emotional attachments (gearheads can be pretty sentimental), the process for securing a private sale on your used vehicle can be a slow and cumbersome process, with a raft of questions along the way. </p>
		    	</div>
	    	</div>
    	</div>
	</div>
</section>

<section id="tab-section" class="tab-section">
	<div class="grid-container">
    	<div class="grid-x grid-padding-x align-center">
	    	<div class="cell large-12 text-center">
		    	<h2 id="tab-section-text-1">Where Should I Sell My Pre-Owned Vehicle?</h2>
		    	
		    	<p id="tab-section-text-2">Ever seen a dusty old junker with a For Sale sign stuck to the front windshield and wondered who’s going to buy that? The unfortunate truth is, not all used cars are created equal. If you have a relatively late model then you may well have more options for selling your used vehicle than other people. Depending on the condition, make and model of your used vehicle, you can choose to sell it in a couple of different ways. For example, you could&hellip;</p>
	    	</div>
    	</div>
    	
    	<div class="grid-x grid-padding-x">
	    	<div class="cell large-12" data-equalizer>
		    	<ul class="accordion" id="sellers-tabs" data-responsive-accordion-tabs="accordion medium-tabs">
			    	<li class="tabs-title">
			    		<a href="#panel1">
				    		<span>Trade-in or Sell <br>to a Dealership</span>
				    		
				    		<span data-equalizer-watch>If the idea of writing an appealing ad, meeting with different buyers, allowing test drives, and haggling for hours on prices sounds like far too much work then the option of talking to a car dealer is always a great option.</span>
				    		
				    		<span class="cta">More</span>
				    	</a>
			    	</li>
			    	
			    	<li class="tabs-title">
			    		<a href="#panel2">
				    		<span>New and Used <br>Vehicle Websites</span>
				    		
				    		<span data-equalizer-watch>As one of the oldest and most well-established car marketplaces on the internet, websites like Autotrader are often the first place private sellers go to list their used vehicles; and on the surface, it doesn’t seem like a bad option at all.</span>
				    		
				    		<span class="cta">More</span>
				    	</a>
			    	</li>
			    	
			    	<li class="tabs-title">
			    		<a href="#panel3">
				    		<span>Local <br>Classifieds</span>
				    		
				    		<span data-equalizer-watch>Craigslist or Kijiji are other well-known websites for selling used cars, used trucks or used SUV’s. Just like Autotrader, these websites offer consistent traffic which makes it far more likely that someone will view your used vehicle and want to buy it.</span>
				    		
				    		<span class="cta">More</span>
				    	</a>
			    	</li>
		    	</ul>
		    	
		    	<div class="tabs-content" data-tabs-content="sellers-tabs">
					<div class="tabs-panel" id="panel1">
						<p>If the idea of writing an appealing ad, meeting with different buyers, allowing test drives, and haggling for hours on prices sounds like far too much work then the option of talking to a car dealer is always a great option.</p>
						
						<p>These transactions are generally a breeze to arrange as most dealerships will have extensive experience handling paperwork and negotiations on similar purchases. Additionally, superficial damage and minor repairs aren’t usually a stumbling block for these buyers as dealerships often have links to high-quality mechanics that can get necessary servicing done in short order.  Not to mention that dealers are always looking for quality pre-owned vehicles.   If you’re looking to purchase a new vehicle from the same dealer, then you might even gain some extra leverage when it comes time to setting a price on your new set of wheels. Trade-ins are also taxed at a lower amount, which can help to offset the cost of purchase.</p>
						
						<p>However, all that convenience does come with its fair share of disadvantages. Because dealerships buy vehicles with a view to sell them on, expect a slightly lower offer from a dealer vs. a private sale.  That said, there are many dealers out there that are willing to pay top dollar for the right vehicle, in some cases they’ll even pay close to retail.  The real benefits of selling your used car, used truck or used SUV to a dealer is that you simply avoid all of the hassles of selling your used vehicle privately, and dealers will buy your quality pre-owned vehicle today.</p>
						
						<div class="text-center">
							<a class="cta" rel="close-tab">Close</a>
						</div>
					</div>
					
					<div class="tabs-panel" id="panel2">
						<p>As one of the oldest and most well-established car marketplaces on the internet, websites like Autotrader are often the first place private sellers go to list their used vehicles; and on the surface, it doesn’t seem like a bad option at all.</p>
						
						<p>Because of the popularity of the website, it does attract a good amount of traffic from all over North America, so chances of your car being seen are very good. These websites also have relatively lax moderation on the used vehicles listed, so even if your used car has titling issues or extensive damage, you’re still unlikely to be barred from posting your used vehicle on the website.</p>
						
						<p>Because of the popularity of the website, it does attract a good amount of traffic from all over North America, so chances of your car being seen are very good. These websites also have relatively lax moderation on the used vehicles listed, so even if your used car has titling issues or extensive damage, you’re still unlikely to be barred from posting your used vehicle on the website.</p>
						
						<p>For all that, these websites also have some clear drawbacks for serious sellers. Firstly, the website charges marketing fees for vehicles posted, and with the kind of competition you’ll face on the websites crowded marketplace, you’ll likely want to upgrade that package to ensure your vehicle gets seen. The lack of oversight on the site also makes it susceptible to scammers and unreliable buyers. Because the website lists your registered email publically, all of these parties are able to contact you directly, so good luck sorting the wheat from the chaff.</p>
						
						<div class="text-center">
							<a class="cta" rel="close-tab">Close</a>
						</div>
					</div>
					
					<div class="tabs-panel" id="panel3">
						<p>Craigslist or Kijiji are other well-known websites for selling used cars, used trucks or used SUV’s. Just like Autotrader, these websites offer consistent traffic which makes it far more likely that someone will view your used vehicle and want to buy it. You will have to go to a fair bit of trouble to get any serious responses though, because experienced purchasers on classifieds are always on the lookout for scams.</p>
						
						<p>That means you need to post a detailed description of your used vehicle which includes its current condition, ownership history, repair record, mileage, make, model and all other pertinent information. You need to add to these details with a slew of images, showing your car from different angles, and damage should be clearly displayed. As you essentially do all the marketing yourself, some of these classifieds sites don’t charge any advertising fees, which is great.  The search functionality on these sites isn’t the greatest either which may impact the user experience; for instance, if you type in “Honda” chances are that you’ll see lawnmowers and generators in your results.</p>
						
						<p>Apart from all the effort you have to put in to get your ad seen, the dangers of using these sites are numerous. The website is regularly used by scam artists and counterfeiters so you have to be extremely careful in this regard. Any time you reach an agreement with a buyer online, you’ll have to schedule private meetings with them and allow them to take a test drive in your vehicle. That’s a big concern when you’re meeting a complete stranger.</p>
						
						<div class="text-center">
							<a class="cta" rel="close-tab">Close</a>
						</div>
					</div>
		    	</div>
	    	</div>
    	</div>
	</div>
</section>

<section id="pronto" class="pronto">
	<div class="grid-container">
    	<div class="grid-x grid-padding-x align-center-middle">
	    	<div class="cell xsmall-12 medium-12 large-7">
		    	<h2 id="pronto-text-1" class="small-text-center large-text-left">Sell your used car pronto with 123Quanto</h2>
		    	
		    	<p id="pronto-text-2">So, you’re looking for a marketplace that gives you the ability to sell your used car, used truck or used SUV at the best possible price to reputable buyers, with little to no effort involved, and a great user experience? You’re not asking for the impossible because 123Quanto provides just such a solution.</p>
		    	
		    	<p class="lead small-text-center large-text-left" id="pronto-text-3">List for FREE today</p>
	    	</div>
	    	
	    	<div class="cell xsmall-12 medium-8 large-5">
		    	<div class="grid-x align-center-middle">
				    <img src="{{ asset('assets/img/home-phone-1.png') }}" alt="Selling used car Vancouver" id="pronto-phone-1" data-interchange="[{{ asset('assets/img/home-phone-1-small.png') }}, xsmall], [{{ asset('assets/img/home-phone-1-small.png') }}, small], [{{ asset('assets/img/home-phone-1.png') }}, medium], [{{ asset('assets/img/home-phone-1.png') }}, large]">
			    	
					<img src="{{ asset('assets/img/home-phone-2.png') }}" alt="Selling truck Vancouver" id="pronto-phone-2" data-interchange="[{{ asset('assets/img/home-phone-2-small.png') }}, xsmall], [{{ asset('assets/img/home-phone-2-small.png') }}, small], [{{ asset('assets/img/home-phone-2.png') }}, medium], [{{ asset('assets/img/home-phone-2.png') }}, large]">
		    	</div>
	    	</div>
    	</div>
	</div>
</section>

<section id="about-123quanto" class="generic">
	<div class="grid-container">
		<div class="frame">
			<div class="grid-x grid-padding-x">
				<div class="cell xsmall-12 large-6">
					<p>123Quanto is a real-time online automobile auction app that gives you the ability to put your used vehicle up for sale in front of hundreds of interested local dealers in almost no time at all.  123Quanto was built by a group of car enthusiasts who were committed to providing an alternative option for the way used cars, used trucks and used SUV’s are bought and sold. Recognizing that modern private buyers have to go through an incredible amount of hassle to secure a sale, the 123Quanto team have decided to streamline and optimize the process from top to bottom.</p>
				</div>
				
				<div class="cell xsmall-12 large-6">
					<p>With 123Quanto, you can use the intuitive interface to enter in your used car details and snap a few photos in mere minutes. As soon as you’ve got the legwork done, your ad is posted to the website’s listings. The next day, your used vehicle goes up for auction in front a wide range of serious buyers, each focused on securing the best sale possible.  That’s right, no meetings, no showings, no test drives and certainly no scammers; this intelligent system allows you to get the best price possible for your used vehicle.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="download-app" class="download-app-section" data-interchange="[{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, xsmall], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, small], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, medium], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, large]">
	<div class="grid-container">
    	<div class="grid-x grid-padding-x">
	    	<div class="cell xsmall-8 xsmall-offset-2 large-4 large-offset-0 xsmall-order-2 large-order-1 text-center">
				<img src="{{ asset('assets/img/download-app-hand.png') }}" alt="Selling my vehicle Vancouver" id="hand" data-interchange="[{{ asset('assets/img/download-app-hand-small.png') }}, xsmall], [{{ asset('assets/img/download-app-hand-small.png') }}, small], [{{ asset('assets/img/download-app-hand-small.png') }}, medium], [{{ asset('assets/img/download-app-hand.png') }}, large]" class="hand">
	    	</div>
		    	
		    <div class="cell xsmall-12 xsmall-offset-0 large-6 large-offset-2 xsmall-order-1 large-order-2">
			    <div class="grid-x padding-x align-middle">
				    <div class="cell text-center">
					    <h2 id="download-app-text-1">Download the app now!</h2>
					    	
					    <p id="download-app-text-2">Serving Greater Vancouver and the Fraser Valley</p>
					    	
				    	<ul class="download-badge" id="download-123quanto-download-badge">
                            <li>
                                <a href="https://itunes.apple.com/ca/app/123quanto/id1251855144?mt=8">
                                	<img src="{{ asset('assets/img/app_store_badge.svg') }}" width="165" height="40" alt="Download 123Quanto app for iOS">
                                </a>
                            </li>

                            <li>
                                <a href="https://play.google.com/store/apps/details?id=com.bilinedev.quanto&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
                                	<img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android">
                                </a>
                            </li>
                        </ul>
                        
                        <p id="download-app-text-3"><small>Attention Dealers: All Dealers are required to register via our website <a href="https://auction.123quanto.com/signin">here</a>.<br>All Dealer accounts must be approved by 123Quanto. You will be notified upon approval.</small></p>
							
						<div class="text-center">
                            <p id="download-app-text-4">Share</p>
                                
							<ul class="share" id="download-app-section-share">
								<li>
			                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url('/download-app')) }}"  class="socmed-share">
			                        	<i class="fa fa-facebook-square fa-2x"></i>
			                        </a>
			                    </li>

			                    <li>
			                        <a href="https://twitter.com/home?status=Download%20the%20123Quanto%20app%20today.%20{{ urlencode(url('/download-app')) }}"  class="socmed-share">
			                        	<i class="fa fa-twitter fa-2x"></i>
			                        </a>
			                    </li>
							</ul>
				    	</div>
			    	</div>
		    	</div>
	    	</div>
		</div>
	</div>
</section>

<section id="appraisals" class="seller appraisals">
	<div class="background">
    	<div class="grid-container">
	    	<div class="grid-x grid-padding-x">
		    	<div class="cell large-6 text-center">
			    	<h2 id="appraisal-text-1">Appraisals</h2>
			    	
			    	<p id="appraisal-text-2">Getting the right value for your pre-owned vehicle can be a difficult balancing act. You don’t want to price yourself out of a ready sale, nor do you want to be taken advantage of. When it comes to getting that valuation, there’s a lot of factors to take into account, including:</p>
		    	</div>
	    	</div>
    	</div>
	</div>
</section>

<section id="assessment" class="assessment">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell large-12">
		    	<div class="grid-x grid-padding-x xsmall-up-1 medium-up-2 large-up-4 item-container" id="assessments" data-equalizer>
			    	<div class="cell">
				    	<div class="item" data-equalizer-watch>
					    	<p>Condition</p>
				    	</div>
			    	</div>
			    	
			    	<div class="cell">
				    	<div class="item" data-equalizer-watch>
					    	<p>VIN</p>
				    	</div>
			    	</div>
			    	
			    	<div class="cell">
				    	<div class="item" data-equalizer-watch>
					    	<p>Maintenance Required</p>
				    	</div>
			    	</div>
			    	
			    	<div class="cell">
				    	<div class="item" data-equalizer-watch>
					    	<p>Recalls on Vehicle</p>
				    	</div>
			    	</div>
			    	
			    	<div class="cell">
				    	<div class="item" data-equalizer-watch>
					    	<p>Model</p>
				    	</div>
			    	</div>
			    	
			    	<div class="cell">
				    	<div class="item" data-equalizer-watch>
					    	<p>Popularity</p>
				    	</div>
			    	</div>
			    	
			    	<div class="cell">
				    	<div class="item" data-equalizer-watch>
					    	<p>Ubiquity on the market</p>
				    	</div>
			    	</div>
			    	
			    	<div class="cell">
				    	<div class="item" data-equalizer-watch>
					    	<p>Selling price of comparable vehicle</p>
				    	</div>
			    	</div>
		    	</div>
			</div>
			
			<div class="cell large-12 text-center">
				<p id="assessment-text-1">Objectively making all of those assessments can be extremely difficult, especially for a first-time seller. At 123Quanto, the appraisal process is made simple. You just enter in the make and model of your used vehicle alongside any other details, and the platform delivers a fair valuation from local dealers in no time at all. </p>
				
				<h3 id="assessment-text-2">Knowing your value is half the battle.</h3>
			</div>
		</div>
	</div>
</section>

<section id="things-to-know" class="things-to-know">
		<div class="grid-container">
			<div class="grid-x">
				<div class="cell large-12 text-center">
					<h2 id="things-to-know-text">Tips for a Private Sale<br>from 123Quanto</h2>
					
					<a class="cta" id="things-to-know-cta" href="{{ url('tips') }}">Click to learn</a>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('page-scripts')
<script>
	$(document).on('click', '[rel="close-tab"]', function(e)
	{
		var tab = $(e.target).closest('.tabs-panel');
        var accordion = $(e.target).closest('.accordion-content');
		
		$(tab).removeClass('is-active');
		
		$('#sellers-tabs').find('li').removeClass('is-active');
		
		$('#sellers-tabs').find('a').attr('aria-selected', "false").attr('aria-expanded', "false");
        
        $(accordion).attr('aria-hidden', true).removeAttr('style');
	});
	
	var tab_counter = 0;
	$('#sellers-tabs').find('.tabs-title').each(function()
	{
		$(this).prop('style', 'transition-delay:'+ ((tab_counter * 0.125) + 0.625)  + 's');
		tab_counter++;
	});
	
	var assessment_counter = 0;
	$('#assessments').find('.cell').each(function()
	{
		$(this).prop('style', 'transition-delay:'+ ((assessment_counter * 0.125) + 1.5)  + 's');
		assessment_counter++;
	});
	
	//
	var scene1_controller = new ScrollMagic.Controller();
	
	var scene1 = new ScrollMagic.Scene({
		triggerElement: "section#seller",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#seller").addClass('is-in-view');
	})
	.addTo(scene1_controller);
	
	//
	var scene2_controller = new ScrollMagic.Controller();
	
	var scene2 = new ScrollMagic.Scene({
		triggerElement: "section#tab-section",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.offset(-100)
	.on("enter", function(){
		$("section#tab-section").addClass('is-in-view');
	})
	.addTo(scene2_controller);
	
	//
	var scene3_controller = new ScrollMagic.Controller();
	
	var scene3 = new ScrollMagic.Scene({
		triggerElement: "section#pronto",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#pronto").addClass('is-in-view');
	})
	.addTo(scene3_controller);
	
	//
	var scene4_controller = new ScrollMagic.Controller();
	
	var scene4 = new ScrollMagic.Scene({
		triggerElement: "section#about-123quanto",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#about-123quanto").addClass('is-in-view');
	})
	.addTo(scene4_controller);
	
	//
	var scene5_controller = new ScrollMagic.Controller();
	
	var scene5 = new ScrollMagic.Scene({
		triggerElement: "section#download-app",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#download-app").addClass('is-in-view');
	})
	.addTo(scene5_controller);
	
	//
	var scene6_controller = new ScrollMagic.Controller();
	
	var scene6 = new ScrollMagic.Scene({
		triggerElement: "section#appraisals",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.offset(-100)
	.on("enter", function(){
		$("section#appraisals").addClass('is-in-view');
		
		$("section#assessment").addClass('is-in-view');
	})
	.addTo(scene6_controller);
	
	//
	var scene8_controller = new ScrollMagic.Controller();
	
	var scene8 = new ScrollMagic.Scene({
		triggerElement: "section#things-to-know",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#things-to-know").addClass('is-in-view');
	})
	.addTo(scene8_controller);
</script>
@endpush
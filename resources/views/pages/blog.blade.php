@extends('layouts._blog')

@push('page-meta')
	<title>The Open Road - 123Quanto Blog</title>
@endpush

@push('body-class')
	<body id="blog-page">
@endpush

@section('content')
	<section class="hero-image grid-x" data-interchange="[assets/img/background-hero-image-home-large.jpg, xsmall], [assets/img/background-hero-image-home-large.jpg, small], [assets/img/background-hero-image-home-large.jpg, medium], [assets/img/background-hero-image-home-large.jpg, large]">
		<div class="cell text-center">
			<h1>The Open Road</h1>

			<p>Articles for sellers, dealers and auto enthusiasts</p>
		</div>
	</section>

	@if (isset($latest_post) && isset($remaining_posts))
		<section class="blog-list">
			<div class="grid-container">
				<div class="grid-x grid-padding-x">
					<div class="cell xsmall-12 large-9">

						<div class="grid-x grid-padding-x blog-items" id="blog-items-1">
							<div class="cell large-12 item large">
								@if ($latest_post->featured_image)
									<img src="{{ asset(imageFly('storage/' . $latest_post->featured_image->path, [870, 327])) }}" width="100%" alt="{{ $latest_post->title }}">
								@endif
								<div class="container">
									<div class="box">
										<p class="title">
											<a href="{{ route('blog.article', $latest_post) }}">
												
											</a>
										</p>
										
										<p>{{ $latest_post->excerpt }}</p>
										
										<footer>
											@include('common.blog.love', ['data' => $latest_post])

											<p class="share">
												@component('common.buttons.share.facebook')
													@slot('data', $latest_post)
												@endcomponent

												@component('common.buttons.share.twitter')
													@slot('post', $latest_post)
												@endcomponent

												@component('common.buttons.share.linkedin')
													@slot('post', $latest_post)
												@endcomponent
											</p>
										</footer>
									</div>
								</div>
							</div>
						</div>	
						
						@foreach ($remaining_posts->chunk(3) as $chunked_remaining_posts)
							<div data-equalizer="excerpt">
								<div class="grid-x grid-padding-x xsmall-up-1 medium-up-2 large-up-3 blog-items" 
									id="blog-items-{{ ($loop->iteration + 1) }}" data-equalizer="title">
									@foreach ($chunked_remaining_posts as $post)
										<div class="cell item small">
											@if ($post->featured_image)
												<img src="{{ asset(imageFly('storage/' . $post->featured_image->path, [400, 150])) }}" width="100%" alt="{{ $post->title }}">
											@endif
											
											<p class="title" data-equalizer-watch="title">
												<a href="{{ route('blog.article', $post) }}">
													{{ $post->title }}
												</a>
											</p>
											
											<p data-equalizer-watch="excerpt">{{ $post->excerpt }}</p>

											<footer>
												@include('common.blog.love', ['data' => $post])

												<p class="share">
													@component('common.buttons.share.facebook')
														@slot('data', $post)
													@endcomponent
		
													@component('common.buttons.share.twitter')
														@slot('post', $post)
													@endcomponent

													@component('common.buttons.share.linkedin')
														@slot('post', $post)
													@endcomponent
												</p>
											</footer>
										</div>
									@endforeach
								</div>
							</div>
						@endforeach
						
						<div class="grid-x grid-padding-x">
							<div class="cell text-center">
								{{ $remaining_posts->links('common.pagination.blog') }}
							</div>
						</div>
					</div>
					
					<aside class="blog cell large-3 hide-for-xsmall-only hide-for-small-only hide-for-medium-only">
						@include('layouts.blog-aside', ['tags' => $tags, 'categories' => $categories])
					</aside>
				</div>
			</div>
		</section>
    @endif
@endsection


@push('page-meta')
	@include('common.meta.facebook', ['post' => $post ?? null])
@endpush

@push('body-scripts')
	{{--  Load Facebook SDK for JavaScript  --}}
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1946661455653025&autoLogAppEvents=1';
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
@endpush

@push('page-scripts')
<script>
	var counter = 0;

	$('.blog-items').each(function()
	{
		var element = $(this).attr('id');
		
		var item_counter = 0;
		$(this).find('.item').each(function()
		{
			$(this).prop('style', 'transition-delay:'+ (item_counter * 0.125)  + 's');
			item_counter++;
		});
		
		var controller = new ScrollMagic.Controller();
		
		var scene = new ScrollMagic.Scene({
			triggerElement: "#" + element,
			//triggerHook: "onEnter",
			duration: "100%"
			
		})
		//.addIndicators()
		.on("enter", function(){
			$("#" + element).addClass('is-in-view');
		})
		.addTo(controller);
		
		counter++;
		
	});
</script>
@endpush

@push('page-scripts')
	<script>window.twttr = (function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0],
		t = window.twttr || {};
	if (d.getElementById(id)) return t;
	js = d.createElement(s);
	js.id = id;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);

	t._e = [];
	t.ready = function(f) {
		t._e.push(f);
	};

	return t;
	}(document, "script", "twitter-wjs"));</script>
@endpush
@extends('layouts._base')

@push('page-meta')
<title>Contact Us - 123Quanto</title>
@endpush

@push('body-class')
<body id="contact-page">
@endpush

@section('content')
<section id="hero-image" class="hero-image" data-interchange="[{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, xsmall], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, small], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, medium], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, large]">
	<div class="grid-container">
    	<div class="grid-x grid-padding-x">
	    	<div class="cell xsmall-12 large-6" id="left">
		    	<h1>How can we make our Service better for you?</h1>
		    	
		    	<p>123Quanto understands that our users are the reason we exist.</p> 

				<p>Please help make our service better by offering your valuable feedback. </p>
				
				<p>Thank you from <img src="{{ asset('assets/img/logo-123quanto-white.png') }}" alt="Car for Sale Vancouver"></p>
				
				<ul class="share">
					<li>Tell your friends about us</li>
					
					<li>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url('/download-app')) }}" class="socmed-share">
                        	<i class="fa fa-facebook-square fa-2x"></i>
                        </a>
                    </li>

                    <li>
                        <a href="https://twitter.com/home?status=Download%20the%20123Quanto%20app%20today.%20{{ urlencode(url('/download-app')) }}" class="socmed-share">
                        	<i class="fa fa-twitter fa-2x"></i>
                        </a>
                    </li>
				</ul>
	    	</div>
	    	
	    	<form class="cell xsmall-12 xsmall-offset-0 large-5 large-offset-1" id="right">
		    	<div class="grid-x grid-padding-x">
			    	<div class="cell medium-6">
				    	<label>First name
				    		<input type="text" placeholder="Required field">
				    	</label>
			    	</div>
			    	
			    	<div class="cell medium-6">
				    	<label>Last name
				    		<input type="text" placeholder="Required field">
				    	</label>
			    	</div>
		    	</div>
		    	
		    	<div class="grid-x grid-padding-x">
			    	<div class="cell large-12">
				    	<label>Email
				    		<input type="email" placeholder="Required field">
				    	</label>
			    	</div>
		    	</div>
		    	
		    	<div class="grid-x grid-padding-x">
			    	<div class="cell medium-6">
				    	<legend>I am a:</legend>
				    	
				    	<div class="faux-input">
					    	<input type="radio" name="type" id="seller-type">
					    	
					    	<label for="seller-type">Seller</label>
				    	</div>
				    	
				    	<div class="faux-input">
					    	<input type="radio" name="type" id="dealer-type">
					    	
					    	<label for="dealer-type">Dealer</label>
				    	</div>
			    	</div>
			    	
			    	<div class="cell medium-6">
				    	<label>Dealership Name
				    		<input type="text">
				    	</label>
			    	</div>
		    	</div>
		    	
		    	<div class="grid-x grid-padding-x">
			    	<div class="cell large-12">
				    	<label>Message
				    		<textarea placeholder="Required field" rows="5"></textarea>
				    	</label>
			    	</div>
		    	</div>
		    	
		    	<div class="grid-x grid-padding-x">
			    	<div class="cell large-12 text-right">
				    	<button class="cta">Submit</button>
			    	</div>
		    	</div>
	    	</form>
    	</div>
	</div>
</section>
@endsection

@push('page-scripts')
<script>
	//
	var scene1_controller = new ScrollMagic.Controller();
	
	var scene1 = new ScrollMagic.Scene({
		triggerElement: "section#hero-image",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#hero-image").addClass('is-in-view');
	})
	.addTo(scene1_controller);
</script>
@endpush
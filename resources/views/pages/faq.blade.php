@extends('layouts._base')

@push('page-meta')
<title>123Quanto Frequently Asked Questions</title>
@endpush

@push('body-class')
<body id="faq-page">
@endpush

@section('content')
<div class="hero-image grid-x grid-padding-x" data-interchange="[assets/img/background-hero-image-home-large.jpg, xsmall], [assets/img/background-hero-image-home-large.jpg, small], [assets/img/background-hero-image-home-large.jpg, medium], [assets/img/background-hero-image-home-large.jpg, large]">
	<div class="cell text-center">
    	<h1>Frequently Asked Questions</h1>
	</div>
</div>
	
<section>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell large-12">
				<ul class="accordion" data-accordion data-allow-all-closed="true">
					<li class="accordion-item-category">Category 1</li>
					
					<li class="accordion-item" data-accordion-item>
						<a href="#" class="accordion-title">Question 1</a>
						
						<div class="accordion-content" data-tab-content>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</li>
					
					<li class="accordion-item" data-accordion-item>
						<a href="#" class="accordion-title">Question 2</a>
						
						<div class="accordion-content" data-tab-content>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</li>
					
					<li class="accordion-item" data-accordion-item>
						<a href="#" class="accordion-title">Question 3</a>
						
						<div class="accordion-content" data-tab-content>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</li>
					
					<li class="accordion-item-category">Category 2</li>
				
					<li class="accordion-item" data-accordion-item>
						<a href="#" class="accordion-title">Question 1</a>
						
						<div class="accordion-content" data-tab-content>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</li>
					
					<li class="accordion-item" data-accordion-item>
						<a href="#" class="accordion-title">Question 2</a>
						
						<div class="accordion-content" data-tab-content>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</li>
					
					<li class="accordion-item" data-accordion-item>
						<a href="#" class="accordion-title">Question 3</a>
						
						<div class="accordion-content" data-tab-content>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
@endsection
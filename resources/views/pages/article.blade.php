@extends('layouts._blog')

@push('page-meta')
	<title>{{ $post->title }} - The Open Road - 123Quanto Blog</title>
@endpush

@push('body-class')
	<body id="blog-page">
@endpush

@section('content')
	<section class="blog-article">
		<div class="grid-container">
			<div class="grid-x grid-padding-x align-center">
				<div class="cell large-12">
					<div class="grid-x grid-padding-x">
						<div class="cell large-9">
							<div class="grid-x grid-padding-x">
								<div class="cell large-12 item large">
									@if ($post->featured_image)
										<img src="{{ asset('storage/' . $post->featured_image->path) }}">
									@endif
									
									<h2>{{ $post->title }}</h2>
									
									<div class="hide author">
										<div class="avatar">
											<img src="{{ asset('assets/img/avatar.jpg') }}">
										</div>
										
										<p>{{ $post->user->name }}</p>
									</div>
									
									<div class="statistic">
										@include('common.blog.love', ['data' => $post])
										
										<p class="share">
											@component('common.buttons.share.facebook')
												@slot('data', $post)
											@endcomponent

											@component('common.buttons.share.twitter')
												@slot('post', $post)
											@endcomponent

											@component('common.buttons.share.linkedin')
												@slot('post', $post)
											@endcomponent
										</p>
									</div>
									
									{!! $post->body !!}
									
									<div class="statistic">
										@include('common.blog.love', ['data' => $post])
										
										<p class="share">
											@component('common.buttons.share.facebook')
												@slot('data', $post)
											@endcomponent

											@component('common.buttons.share.twitter')
												@slot('post', $post)
											@endcomponent

											@component('common.buttons.share.linkedin')
												@slot('post', $post)
											@endcomponent
										</p>
									</div>
								</div>
							</div>
							<hr>
						</div>
						
						<aside class="blog cell large-3 hide-for-xsmall-only hide-for-small-only">
							@include('layouts.blog-aside', ['tags' => $tags, 'categories' => $categories])
						</aside>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="blog-list">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell large-12">
					<h3>More from our blog</h3>
					
					<div class="grid-x grid-padding-x large-up-4">
						@foreach ($more_posts as $post)
							<div class="cell item small">
								@if ($post->featured_image)
									<img src="{{ asset(imageFly('storage/' . $post->featured_image->path, [400, 150])) }}" width="100%" alt="{{ $post->title }}">
								@endif
								
								<p class="title">
									<a href="{{ route('blog.article', $post) }}">
										{{ $post->title }}
									</a>
								</p>
								
								<p>{{ $post->excerpt }}</p>
								
								<footer>
									@include('common.blog.love', ['data' => $post])
									
									<p class="share">
										@component('common.buttons.share.facebook')
											@slot('data', $post)
										@endcomponent

										@component('common.buttons.share.twitter')
											@slot('post', $post)
										@endcomponent
									</p>
								</footer>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection


@push('page-meta')
	@include('common.meta.facebook', ['post' => $post ?? null])
@endpush

@push('body-scripts')
	{{--  Load Facebook SDK for JavaScript  --}}
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1946661455653025&autoLogAppEvents=1';
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
@endpush

@push('page-scripts')
	<script>window.twttr = (function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0],
		t = window.twttr || {};
	if (d.getElementById(id)) return t;
	js = d.createElement(s);
	js.id = id;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);

	t._e = [];
	t.ready = function(f) {
		t._e.push(f);
	};

	return t;
	}(document, "script", "twitter-wjs"));</script>
@endpush
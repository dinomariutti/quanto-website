@extends('layouts._base')

@push('page-meta')
<title>For Dealers - 123Quanto</title>
@endpush

@push('body-class')
<body id="dealers-page">
@endpush

@section('content')
<section id="dealer" class="dealer">
	<div class="background">
    	<div class="grid-container">
	    	<div class="grid-x grid-padding-x">
		    	<div class="cell xsmall-12 large-6 text-center">
			    	<h1 id="dealer-text-1">For Dealers</h1>
			    	
			    	<p id="dealer-text-2">With a record 2.038 million cars sold throughout the country, 2017 was something of a banner year for Canada’s auto industry.  As the economy grew and oil prices fell, car owners and first-time buyers alike looked towards brick-and-mortar dealerships to satisfy their need for a fresh set of wheels. </p>
		    	</div>
	    	</div>
    	</div>
	</div>


	<div class="generic">
		<div class="grid-container">
			<div class="grid-x">
		    	<div class="cell large-12 text-center">
			    	<p>But just because Canadians are willing to make big-ticket purchases, doesn’t mean they’re ready to pay over the odds.  Increasingly, savvy customers are looking towards the used car market for great deals on like-new vehicles; and the growing focus on pre-owned vehicles is hardly a new trend. According to leading auto industry indices, wholesale market values for used vehicles have grown 3.4% year-on-year for almost a decade now, on the back of sustained retail demand. </p>
			    	
			    	<p>Interestingly much of this growth is being fueled by car sales for vehicles that are under 4 years old. With easily available leasing and attractive financing offers littering the industry, car owners are more than willing to trade in, or upgrade their lightly used vehicles; which means a constant supply of late model cars now available on the market. For buyers looking to splurge, it’s an opportunity that’s too good to pass up.</p>
			    	
			    	<p>Whether you’re an established car dealer or a relative novice to the industry, the market for pre-owned vehicles represents a highly lucrative proposition; but alongside that potential for reward, lays a variety of risks that you’ll need to be well aware of.</p>
		    	</div>
			</div>
		</div>
</div>
</section>

<section id="tab-section" class="tab-section">
	<div class="grid-container">
		<div class="grid-x">
	    	<div class="cell large-12 text-center">
		    	<h2 id="tab-section-text-1">Challenges you’ll face in the Used Car Market</h2>
	    	</div>
		</div>
		
		<div class="grid-x">
	    	<div class="cell large-12" data-equalizer>
		    	<ul class="accordion" id="dealers-tabs" data-responsive-accordion-tabs="accordion medium-tabs">
			    	<li class="tabs-title">
			    		<a href="#panel1">
				    		<img src="{{ asset('assets/img/icon-price.png') }}" alt="How to sell you SUV Vancouver">
				    		
				    		<span data-equalizer-watch>Pricing</span>
				    		
				    		<span class="cta">More</span>
			    		</a>
			    	</li>
			    	
			    	<li class="tabs-title">
			    		<a href="#panel2">
				    		<img src="{{ asset('assets/img/icon-demands.png') }}" alt="Public Auto Auction Vancouver">
				    		
				    		<span data-equalizer-watch>Specific <br>Demands</span>
				    		
				    		<span class="cta">More</span>
			    		</a>
			    	</li>
			    	
			    	<li class="tabs-title">
			    		<a href="#panel3">
				    		<img src="{{ asset('assets/img/icon-negotiate.png') }}" alt="Online auto auction Vancouver">
				    		
				    		<span data-equalizer-watch>Negotiations</span>
				    		
				    		<span class="cta">More</span>
			    		</a>
			    	</li>
			    	
			    	<li class="tabs-title">
			    		<a href="#panel4">
				    		<img src="{{ asset('assets/img/icon-fluctuations.png') }}" alt="Tips for selling my used suv Vancouver">
				    		
				    		<span data-equalizer-watch>Market <br>Fluctuations</span>
				    		
				    		<span class="cta">More</span>
			    		</a>
			    	</li>
		    	</ul>
		    	
		    	<div class="tabs-content" data-tabs-content="dealers-tabs">
					<div class="tabs-panel" id="panel1">
						<p>The rise of the internet has brought an unprecedented level of transparency to the auto industry. Where dealers could once get away with charging consistent mark-ups on their vehicles, they now have to contend with far more informed buying public.  To compete with other brick-and mortar retailers and online marketplaces, dealerships must now price and promote their vehicles in a market-rational manner.  Of course, this sort of prudent pricing strategy could significantly impact your forecasted bottom line, so it will necessary to identify opportunities for cost-saving elsewhere in the acquisition process.</p>
						
						<div class="text-center">
							<a class="cta" rel="close-tab">Close</a>
						</div>
					</div>
					
					<div class="tabs-panel" id="panel2">
						<p>Nowadays, when a prospective buyer wanders into your dealership, they aren’t looking to be sold on a variety of cars in your inventory. In fact, in most cases they’ve already done this research online beforehand, which means they come into negotiations with a wealth of information regarding the make, model and features they desire in their prospective purchase. Essentially at this point your function is to connect your customer with the vehicle they want, at the value they’re willing to pay; and if you’re unable to serve this demand, then your customer may well continue their search elsewhere.</p>
						
						<p>Instead of making scattershot purchasing decisions, dealerships must now apply research and statistical analysis towards finding and acquiring the right vehicles to meet consumer demands.  The more optimized your stock is, to suit specific market preferences, the higher your turnover will be.</p>
						
						<div class="text-center">
							<a class="cta" rel="close-tab">Close</a>
						</div>
					</div>
					
					<div class="tabs-panel" id="panel3">
						<p>Increasingly dealerships are adopting fixed price sales models, where they will set a market-consistent price on their vehicles and simply wait on the first serious offer.  In the digital age, customers simply aren’t willing to waste long hours on back and forth haggling, when such discussions can easily be initiated and completed online, before a meeting even takes place. Car buyers are looking for a fast, effective sales process, and they’re more than willing to shop around for the right dealership. </p>
						
						<p>With that in mind, dealerships need to look towards sales volume over individual profit margins if they’re going to continue to be competitive in the next decade.</p>
						
						<div class="text-center">
							<a class="cta" rel="close-tab">Close</a>
						</div>
					</div>
					
					<div class="tabs-panel" id="panel4">
						<p>While the used car market has been steadily expanding for years now, economic conditions may not always be so conducive. The tail-end of 2017 saw the loonie trade at 80 cents on the US dollar, as a result much of the US demand for used Canadian vehicles slowed, with that decrease in overseas interest came a commiserate fall in local used auto prices. Similarly, while financing is cheaply available at the moment, a rise in overnight rates is pretty much expected within the next five years, which will in turn dampen appetites for large purchases.</p>
						
						<p>Dealerships need to start making preparations for a variety of scenarios now; and for most, that means optimizing purchasing processes to make them as cost-effective as possible.</p>
						
						<div class="text-center">
							<a class="cta" rel="close-tab">Close</a>
						</div>
					</div>
		    	</div>
	    	</div>
		</div>
	</div>
</section>

<section id="inventory" class="inventory" data-interchange="[assets/img/background-download-large.jpg') }}, xsmall], [assets/img/background-download-large.jpg') }}, small], [assets/img/background-download-large.jpg') }}, medium], [assets/img/background-download-large.jpg') }}, large]">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell xsmall-12 large-6">
				<h2 class="text-center large-text-left" id="inventory-text-1">Inventory is King</h2>
				
				<p id="inventory-text-2">A vehicle that isn’t generating interest from the day it arrives on the lot will only depreciate further in value with age. Despite this fact, far too many dealerships take their chances by purchasing distressed  vehicles at cut-price rates in the hope that they’ll be able to turn an unlikely profit with the proper sales skills.  If you truly want to optimize your buying and selling processes then you need to treat inventory acquisition, and management as a function all of its own.</p>
			</div>
			
			<div class="cell xsmall-12 large-6">
				<h4 id="inventory-text-3">You need:</h4>

				<p id="inventory-text-4">A thorough understanding of your local market, complete with sales trends for different brands, makes and models of vehicle.  This data must form the basis of a focused acquisition strategy.</p>

				<p id="inventory-text-5">A systematic framework in place for evaluating prospective purchases. Used cars must be evaluated for their ondition, configuration, added features and sales potential.</p>

				<p id="inventory-text-6">To determine whether reconditioning is required to secure a retail sale. If it is, the reconditioning cost must be calculated beforehand and tallied into the expected profit margin for the vehicle.</p> 

				<p id="inventory-text-7">A consistent, diverse supply of inventory which can be easily sourced and negotiated at minimum cost. The ideal inventory should consist of several good-to-great condition, low-end, late-model vehicles; as these vehicles will present the greatest opportunity for quick turnover and high profit margins.</p> 
			</div>
		</div>
	</div>
</section>

<section id="where-to-source" class="generic">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
	    	<div class="cell large-12 text-center">
		    	<h2 id="where-to-source-text-1">Where to source?</h2>
		    	
		    	<p id="where-to-source-text-2">In a DealerSocket survey of 2000 independent used car dealers, the majority identified sourcing issues as the number one problem they faced in their business. Simply put small to mid-sized dealerships just aren’t able to find desirable vehicles through traditional means, without paying a huge premium in additional fees.  This is particularly worrying; when you consider the fact that the majority of used car businesses are still relying on wholesale auctions and brand dealerships for most of their inventory acquisitions. </p>
	    	</div>
		</div>
		
		<div class="frame"  id="where-to-source-frame">
			<div class="grid-x grid-padding-x">
				<div class="cell xsmall-12 large-6">
					<h3 id="where-to-source-text-3">Wholesale Auctions</h3>
					
					<p id="where-to-source-text-4">While you can pick up discounts of up to 50% on market value in auction purchases, there is a host of additional concerns that comes alongside.  Many of the vehicles up for sale on these lots are repossessed or confiscated, because of which it is often difficult to ascertain the true ownership history of the car.  Additionally, most of these cars will come without any sort of warranty or insurance documentation, creating further issues when it comes time to sell them on.  Mechanical deficiencies and superficial damages are also quite common with wholesale pieces, so a degree of caution is definitely advised. Finally, bidders need to be aware of the often exorbitant fees which are attached to auction purchases; these hidden payments can sap the value from even the best deal.</p>
				</div>
				
				<div class="cell xsmall-12 large-6">
					<h3 id="where-to-source-text-5">Other Dealerships</h3>
					
					<p id="where-to-source-text-6">In other cases, used car dealers may be able to pick up unwanted trade-ins at an agreeable price from franchise dealerships.  The issue with these acquisitions is often limited supply. While you may be able to find a few great deals on desirable cars, optimizing these purchases for value and time requires concerted effort. Although these purchases are certainly a great way to augment existing stock, the inefficiencies they introduce into the buying process mean you cannot rely on them for the majority of your sourcing strategy. </p>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="business" class="business">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell xsmall-12 large-6">
				<h2 class="text-center large-text-left" id="business-text-1">A New Way of Doing Business</h2>
				
				<p id="business-text-2">123Quanto is a new lead generation tool, developed by a group of car enthusiasts and car dealers.  At its core, the platform seeks to deliver an expanded view of the used vehicle market to dealerships. Through an intuitive interface, the 123Quanto app offers instant access to used vehicle inventory and potential buyers in a variety of locations.</p>
				
                <div class="text-center">
                    <p id="business-text-3">Share</p>
                    
                    <ul class="share" id="business-section-share">
                        <li>
	                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url('/download-app')) }}" class="socmed-share">
	                        	<i class="fa fa-facebook-square fa-2x"></i>
	                        </a>
	                    </li>

	                    <li>
	                        <a href="https://twitter.com/home?status=Download%20the%20123Quanto%20app%20today.%20{{ urlencode(url('/download-app')) }}" class="socmed-share">
	                        	<i class="fa fa-twitter fa-2x"></i>
	                        </a>
	                    </li>
                    </ul>
                </div>
			</div>
			
			<div class="cell xsmall-12 large-6 text-center">
				<p class="lead" id="business-text-4">Download the App and source inventory for free today!</p>
				
				<ul class="download-badge" id="business-download-badge">
                    <li>
                        <a href="https://itunes.apple.com/ca/app/123quanto/id1251855144?mt=8">
                        	<img src="{{ asset('assets/img/app_store_badge.svg') }}" width="165" height="40" alt="Download 123Quanto app for iOS">
                       	</a>
                    </li>

                    <li>
                        <a href="https://play.google.com/store/apps/details?id=com.bilinedev.quanto&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
                        	<img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android">
                        </a>
                    </li>
                </ul>
                
                <p id="business-text-5"><small>Attention Dealers: All Dealers are required to register via our website <a href="https://auction.123quanto.com/signin">here</a>.<br>All Dealer accounts must be approved by 123Quanto. You will be notified upon approval.</small></p>
			</div>
		</div>
	</div>
</section>

<section id="independent-dealers" class="generic">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell xsmall-12 large-6">
				<p id="independent-dealers-text-1">For independent dealers, that means extreme optimization at both ends of the business. Not only do you get a superior dedicated platform for finding and sourcing quality used vehicles without leaving the office, but you also get to connect with a wide array of interested buyers in an extremely convenient way.  With 123Quanto you get to expand your inventory hunt past the local market, and with those added options you gain the ability to target specific units at the right condition and price, with NO BUYING FEES.</p>
			</div>
			
			<div class="cell xsmall-12 large-6">
				<p id="independent-dealers-text-2">If you’re looking to reach out to sellers that are also for a new vehicle, 123Quanto’s app provides excellent opportunities to do so.  Once you’ve identified a good purchasing opportunity, talk to the seller and see if you can agree on a trade-in amount - chances are that the seller will buy a new vehicle as well.</p>
				
				<p id="independent-dealers-text-3">The best part is that all of these features come completely free. That means no auction fees, no finder fees, and no commissions to uninvolved third parties. What more could a dealership ask for?</p>
			</div>
		</div>
	</div>
</section>

<section id="download-app" class="download-app-section" data-interchange="[{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, xsmall], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, small], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, medium], [{{ asset('assets/img/background-hero-image-download-app-large.jpg') }}, large]">
	<div class="grid-container">
    	<div class="grid-x grid-padding-x">
	    	<div class="cell xsmall-8 xsmall-offset-2 large-4 large-offset-0 xsmall-order-2 large-order-1">
				<img src="{{ asset('assets/img/download-app-hand.png') }}" class="hand" id="download-app-hand" alt="Trade in my truck Vancouver" data-interchange="[{{ asset('assets/img/download-app-hand-small.png') }}, xsmall], [{{ asset('assets/img/download-app-hand-small.png') }}, small], [{{ asset('assets/img/download-app-hand-small.png') }}, medium], [{{ asset('assets/img/download-app-hand.png') }}, large]">
	    	</div>
	    	
	    	<div class="cell xsmall-12 xsmall-offset-0 large-6 large-offset-2 xsmall-order-1 large-order-2">
		    	<div class="grid-x grid-padding-x align-middle">
			    	<div class="cell text-center">
				    	<h2 id="download-app-text-1">Source your inventory pronto with 123Quanto</h2>
				    	
				    	<ul class="download-badge" id="download-app-download-badge">
                            <li>
                                <a href="https://itunes.apple.com/ca/app/123quanto/id1251855144?mt=8">
                                	<img src="{{ asset('assets/img/app_store_badge.svg') }}" width="165" height="40" alt="Download 123Quanto app for iOS">
                                </a>
                            </li>

                            <li>
                                <a href="https://play.google.com/store/apps/details?id=com.bilinedev.quanto&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
                                	<img src="{{ asset('assets/img/google-play-badge.svg') }}" width="135" height="40" alt="Download 123Quanto app for Android">
                                </a>
                            </li>
                        </ul>
                        
                         <p id="download-app-text-2"><small>Attention Dealers: All Dealers are required to register via our website <a href="https://auction.123quanto.com/signin">here</a>.<br>All Dealer accounts must be approved by 123Quanto. You will be notified upon approval.</small></p>
			    	</div>
		    	</div>
	    	</div>
    	</div>
	</div>
</section>
@endsection

@push('page-scripts')
<script>
	$(document).on('click', '[rel="close-tab"]', function(e)
	{
		var tab = $(e.target).closest('.tabs-panel');
        var accordion = $(e.target).closest('.accordion-content');
		
		$(tab).removeClass('is-active');
		
		$('#dealers-tabs').find('li').removeClass('is-active');
		
		$('#dealers-tabs').find('a').attr('aria-selected', "false").attr('aria-expanded', "false");
        
        $(accordion).attr('aria-hidden', true).removeAttr('style');
	});
	
	var tab_counter = 0;
	$('#dealers-tabs').find('.tabs-title').each(function()
	{
		$(this).prop('style', 'transition-delay:'+ ((tab_counter * 0.125) + 0.375)  + 's');
		tab_counter++;
	});
	
	//
	var scene1_controller = new ScrollMagic.Controller();
	
	var scene1 = new ScrollMagic.Scene({
		triggerElement: "section#dealer",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#dealer").addClass('is-in-view');
	})
	.addTo(scene1_controller);
	
	//
	var scene3_controller = new ScrollMagic.Controller();
	
	var scene3 = new ScrollMagic.Scene({
		triggerElement: "section#tab-section",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#tab-section").addClass('is-in-view');
	})
	.addTo(scene3_controller);
	
	//
	var scene4_controller = new ScrollMagic.Controller();
	
	var scene4 = new ScrollMagic.Scene({
		triggerElement: "section#inventory",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#inventory").addClass('is-in-view');
	})
	.addTo(scene4_controller);
	
	//
	var scene5_controller = new ScrollMagic.Controller();
	
	var scene5 = new ScrollMagic.Scene({
		triggerElement: "section#where-to-source",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#where-to-source").addClass('is-in-view');
	})
	.addTo(scene5_controller);
	
	//
	var scene6_controller = new ScrollMagic.Controller();
	
	var scene6 = new ScrollMagic.Scene({
		triggerElement: "section#business",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#business").addClass('is-in-view');
	})
	.addTo(scene6_controller);
	
	//
	var scene6_controller = new ScrollMagic.Controller();
	
	var scene6 = new ScrollMagic.Scene({
		triggerElement: "section#independent-dealers",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.offset(-100)
	.on("enter", function(){
		$("section#independent-dealers").addClass('is-in-view');
	})
	.addTo(scene6_controller);
	
	//
	var scene7_controller = new ScrollMagic.Controller();
	
	var scene7 = new ScrollMagic.Scene({
		triggerElement: "section#download-app",
		//triggerHook: "onEnter",
		duration: "100%"
		
	})
	//.addIndicators()
	.on("enter", function(){
		$("section#download-app").addClass('is-in-view');
	})
	.addTo(scene7_controller);
</script>
@endpush
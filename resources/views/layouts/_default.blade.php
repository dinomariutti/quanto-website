<!doctype html>
<html class="no-js" lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        @stack('page-meta')
		 
        @stack('page-styles')
        
        @include('layouts.styles')
        
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
        
        {{--  Global site tag (gtag.js) - Google Analytics  --}}
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113373655-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-113373655-1');
        </script>
    </head>
    
    @stack('body-class')
        @include('layouts.header-default')
        
        @stack('body-scripts')
    	 
	    <div class="off-canvas-wrapper" data-content-overlay="false">
		    <div class="off-canvas position-left" id="off-canvas-left" data-off-canvas data-content-overlay="false">
			    @include('layouts.off-canvas-main-navigation')
		    </div>
		    
		    <div class="off-canvas-content" data-off-canvas-content>
		        @yield('content')
				
				@include('layouts.footer')
		    </div>
	    </div>
	    
	    @include('layouts.scripts')
				
		@stack('page-scripts')
    </body>
</html>
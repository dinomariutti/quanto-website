<form>
	<div class="input-group">
		<input type="search" placeholder="Search article" class="search">
		
		<div class="input-group-button">
			<button><i class="fa fa-search"></i></button>
		</div>
	</div>
</form>

<form>
	<h4>Subscribe to our newsletter</h4>
	
	<div class="input-group">
		<input type="email" placeholder="Email address" class="subscribe">
		
		<div class="input-group-button">
			<button>Submit</button>
		</div>
	</div>
</form>

<h4>Categories</h4>

<ul class="category-navigation">
	@foreach($categories as $category)
		@if ($category->parent == null)
			<li>
				<a href="{{ route('blogs.by-category', $category) }}">
					{{ $category->name }}
				</a>
				@if ($category->childs->isNotEmpty())
					<ul>
						@foreach($category->childs as $child)
							<li>
								<a href="{{ route('blogs.by-category', $child) }}">
									{{ $child->name }}
								</a>
								@if ($child->childs->isNotEmpty())
									<ul>
										@foreach($child->childs as $child_2)
											<li>
												<a href="{{ route('blogs.by-category', $child_2) }}">
													{{ $child_2->name }}
												</a>
											</li>
										@endforeach
									</ul>
								@endif
							</li>
						@endforeach
					</ul>
				@endif
			</li>
		@endif
	@endforeach
</ul>

<h4>Tags</h4>

<ul class="tags-navigation">
	@foreach($tags as $tag)
		<li>
			<a href="{{ route('blogs.by-tag', $tag) }}">
				{{ $tag->name }}
			</a>
		</li>
	@endforeach
</ul>
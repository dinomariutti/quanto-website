<footer id="main-footer">
	<div class="grid-container">
    	<div class="grid-x">
	    	<div class="cell text-center">
		    	<ul id="footer-navigation">
			    	<li>
			    		<a href="{{ url('about-us') }}">About 123Quanto</a>
			    	</li>
			    	
			    	<li>
			    		<a href="{{ url('sellers') }}">For Sellers</a>
			    	</li>
			    	
			    	<li>
			    		<a href="{{ url('dealers') }}">For Dealers</a>
			    	</li>
			    	
			    	<li>
			    		<a href="{{ url('contact-us') }}">Contact</a>
			    	</li>
		    	</ul>
	    	</div>
    	</div>
	</div>
</footer>
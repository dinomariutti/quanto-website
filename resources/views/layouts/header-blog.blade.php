<header id="main-header">
	<div class="grid-container">
    	<div class="grid-x">
	    	<div class="cell xsmall-12 large-4">
		    	<button class="toggle-off-canvas" data-toggle="off-canvas-left">
		    		<i class="fa fa-bars"></i>
		    	</button>
		    	
		    	<button class="toggle-off-canvas" data-toggle="off-canvas-right">
		    		<i class="fa fa-search"></i>
		    	</button>
		    	
		    	<a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo-123quanto-blue.png') }}" id="main-logo" alt="123Quanto logo"></a>
	    	</div>
	    	
	    	<div class="cell xsmall-12 large-8">
		    	<ul id="main-navigation">
			    	<li @if($page == "how-it-works") class="active" @endif>
			    		<a href="{{ url('how-it-works') }}">How it works</a>
			    	</li>
			    	
			    	<li @if($page == "sellers") class="active" @endif>
			    		<a href="{{ url('sellers') }}">For Sellers</a>
			    	</li>
			    	
			    	<li @if($page == "dealers") class="active" @endif>
			    		<a href="{{ url('dealers') }}">For Dealers</a>
			    	</li>
			    	
			    	<li @if($page == "blog") class="active" @endif>
			    		<a href="{{ url('blog') }}">Blog</a>
			    	</li>
			    	
			    	<li @if($page == "about-us") class="active" @endif>
			    		<a href="{{ url('about-us') }}">About us</a>
			    	</li>
			    	
			    	<li @if($page == "contact-us") class="active" @endif>
			    		<a href="{{ url('contact-us') }}">Contact</a>
			    	</li>
			    	
			    	<li class="login">
			    		<a href="https://auction.123quanto.com/signin">Login</a>
			    	</li>
		    	</ul>
	    	</div>
    	</div>
	</div>
</header>
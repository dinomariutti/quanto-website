<ul id="off-canvas-navigation">
	<li>
		<a href="{{ url('how-it-works') }}">How it works</a>
	</li>
	
	<li>
		<a href="{{ url('sellers') }}">For Sellers</a>
	</li>
	
	<li>
		<a href="{{ url('dealers') }}">For Dealers</a>
	</li>
	
	<li>
		<a href="{{ url('blog') }}">Blog</a>
	</li>
	
	<li>
		<a href="{{ url('about-us') }}">About us</a>
	</li>
	
	<li>
		<a href="{{ url('contact-us') }}">Contact</a>
	</li>
</ul>
<header id="main-header">
	<div class="grid-container">
    	<div class="grid-x">
	    	<div class="cell xsmall-12 large-4">
		    	<button class="toggle-off-canvas" data-toggle="off-canvas-left">
		    		<i class="fa fa-bars"></i>
		    	</button>
		    	
		    	<a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo-123quanto-blue.png') }}" id="main-logo" alt="123Quanto logo"></a>
	    	</div>
	    	
	    	<div class="cell xsmall-12 large-8">
		    	<ul id="main-navigation">
			    	<li>
			    		<a href="{{ url('how-it-works') }}">How it works</a>
			    	</li>
			    	
			    	<li>
			    		<a href="{{ url('sellers') }}">For Sellers</a>
			    	</li>
			    	
			    	<li>
			    		<a href="{{ url('dealers') }}">For Dealers</a>
			    	</li>
			    	
			    	<li>
			    		<a href="{{ url('blog') }}">Blog</a>
			    	</li>
			    	
			    	<li>
			    		<a href="{{ url('about-us') }}">About us</a>
			    	</li>
			    	
			    	<li>
			    		<a href="{{ url('contact-us') }}">Contact</a>
			    	</li>
			    	
			    	<li class="login">
			    		<a href="https://auction.123quanto.com/signin">Login</a>
			    	</li>
		    	</ul>
	    	</div>
    	</div>
	</div>
</header>
<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parent = Category::create(['name' => 'Articles for Dealers']);

        Category::create([
            'parent_id' => $parent->id,
            'name' => 'Inventory Tips'
        ]);

        Category::create([
            'parent_id' => $parent->id,
            'name' => 'Selling Tools and Tips'
        ]);

        $parent = Category::create(['name' => 'Articles for Sellers']);

        Category::create([
            'parent_id' => $parent->id,
            'name' => 'Best of'
        ]);

        Category::create([
            'parent_id' => $parent->id,
            'name' => 'Buying Tips'
        ]);

        Category::create([
            'parent_id' => $parent->id,
            'name' => 'Selling Tips'
        ]);
    }
}

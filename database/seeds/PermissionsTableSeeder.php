<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Assign permissions to 'admin' role
         */
        $admin = Role::where('name', 'admin')->first();

        $admin->givePermissionTo(Permission::create(['name' => 'create-user']));
        $admin->givePermissionTo(Permission::create(['name' => 'edit-user']));
        $admin->givePermissionTo(Permission::create(['name' => 'remove-user']));

        /**
         * Assign permissions to 'editor' role
         */
        $editor = Role::where('name', 'editor')->first();

        /**
         * Assign permissions to 'guest' role
         */
        $guest = Role::where('name', 'guest')->first();
    }
}

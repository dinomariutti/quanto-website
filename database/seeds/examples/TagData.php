<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class TagData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tag::class, 7)->create();
    }
}

<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class CategoryData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Collection::times(3, function ($number) {

            factory(Category::class)->create([
                'name' => 'Parent #00' . $number
            ]);

        });

		Collection::times(5, function ($number) {

            factory(Category::class)->create([
                'parent_id' => array_random([
                    Category::parentCategory()->get()->random()->id
                ])
            ]);

		});

        Collection::times(5, function ($number) {

            factory(Category::class)->create([
                'parent_id' => array_random([
                    Category::has('parent')->get()->random()->id
                ])
            ]);
            
        });
    }
}

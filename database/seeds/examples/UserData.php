<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create an 'editor' user
         */
        $editor = User::create([
            'name' => 'Editor 001',
            'email' => 'editor001@quanto.example',
            'password' => bcrypt('editor')
        ]);

        $editor->assignRole(
            Role::where('name', 'editor')->first()
        );

        /**
         * Create a 'guest' user
         */
        factory(User::class, 5)
            ->create()
            ->each(function ($user) {

                $user->assignRole(
                    Role::where('name', 'guest')->first()
                );

            });
    }
}

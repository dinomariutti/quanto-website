<?php

use Illuminate\Database\Seeder;

class ExampleDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserData::class);
        
        $this->call(CategoryData::class);
        $this->call(TagData::class);
        $this->call(PostData::class);
        $this->call(ImageData::class);
        $this->call(SocialData::class);
        $this->call(LikeData::class);
    }
}

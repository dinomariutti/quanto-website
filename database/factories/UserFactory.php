<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    
    static $password;

    $fist_name = $faker->firstName;
    $last_name = $faker->lastName;

    return [
        'name' => $fist_name . ' ' . $last_name,
        'email' => strtolower($fist_name . '.' . $last_name) . '@quanto.example',
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
    
});

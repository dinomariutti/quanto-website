<?php

use App\User;
use App\Models\Like;
use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Like::class, function (Faker $faker) {

    return [
        'post_id' => function() {
            return factory(Post::class)->create()->id;
        },
        'user_id' => function() use ($faker) {
            return $faker->randomElement([
                null,
                factory(User::class)->create()->id
            ]);
        },
        'ip_address' => $faker->ipv4
    ];

});

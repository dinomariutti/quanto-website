<?php

use App\Models\Post;
use App\Models\Social;
use Faker\Generator as Faker;

$factory->define(Social::class, function (Faker $faker) {

    return [
        'post_id' => function() {
            return factory(Post::class)->create()->id;
        },

        'open_graph_title' => 'Title ' . $faker->sentence,
        'open_graph_sitename' => 'Sitename ' . $faker->sentence,
        'open_graph_post_url' => $faker->url,
        'open_graph_description' => $faker->paragraph,
        'open_graph_path' => null,
        'open_graph_size' => null,
        'open_graph_mime' => null,

        'twitter_card_type' => $faker->randomElement(['summary', 'large image']),
        'twitter_title' => 'Title ' . $faker->sentence,
        'twitter_description' => 'Sitename ' . $faker->sentence,
        'twitter_path' => null,
        'twitter_size' => null,
        'twitter_mime' => null
    ];

});

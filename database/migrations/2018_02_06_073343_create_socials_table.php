<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socials', function (Blueprint $table) {
            $table->increments('id');
            $table->char('uuid', 36)->unique();
            $table->unsignedInteger('post_id');
            
            $table->string('open_graph_title')->nullable();
            $table->string('open_graph_sitename')->nullable();
            $table->string('open_graph_post_url')->nullable();
            $table->text('open_graph_description')->nullable();
            $table->boolean('open_graph_is_use_featured_iamge')->default(false);
            $table->string('open_graph_path')->nullable();
            $table->integer('open_graph_size')->nullable();
            $table->string('open_graph_mime')->nullable();

            $table->string('twitter_card_type')->nullable();
            $table->string('twitter_title')->nullable();
            $table->string('twitter_description')->nullable();
            $table->boolean('twitter_is_use_featured_image')->default(false);
            $table->string('twitter_path')->nullable();
            $table->integer('twitter_size')->nullable();
            $table->string('twitter_mime')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('post_id')
                    ->references('id')->on('posts')
                    ->onUpdate('cascade')
                    ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socials');
    }
}

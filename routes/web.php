<?php

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


////////////////////////////// ADMIN AREA //////////////////////////////

Route::prefix('/admin')->middleware('auth')->group(function () {

    Route::get('/', function () {
            return redirect()->route('dashboard.index');
    })->name('admin.index');

    Route::view('/dashboard', 'backend.dashboard.index')->name('dashboard.index');

    Route::resource('/category', 'CategoryController');
    Route::resource('/tag', 'TagController');
    Route::resource('/post', 'PostController');
    Route::resource('/like', 'LikeController');
    
    Route::resource('/user', 'UserController');
    
});


////////////////////////////// FRONT-END //////////////////////////////

Route::get('/', 'MainController@home');
Route::get('/how-it-works', 'MainController@how_it_works');
Route::get('/sellers', 'MainController@sellers');
Route::get('/dealers', 'MainController@dealers');
Route::get('/blog', 'MainController@blog');
Route::get('/blog/post/{id}', 'MainController@article')->name('blog.article');
Route::get('/about-us', 'MainController@about_us');
Route::get('/contact-us', 'MainController@contact_us');
Route::get('/download-app', 'MainController@download_app');
Route::get('/faq', 'MainController@faq');
Route::get('/tips', 'MainController@tips');

Route::get('/tag/{tag}', 'MainController@blogsByTag')->name('blogs.by-tag');
Route::get('/category/{category}', 'MainController@blogsByCategory')->name('blogs.by-category');

Route::resource('post-like', 'PostLikeController', ['parameters' => ['post-like' => 'post']]);

//
Route::any('/assets/img/{slug}', function(){
    return \Image::canvas(800, 600, '#ff0000')->response('jpg');
});
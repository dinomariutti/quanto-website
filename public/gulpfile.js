var gulp = require('gulp');
var $    = require('gulp-load-plugins')();

var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');

var sassPaths = [
  'bower_components/normalize.scss/sass',
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

gulp.task('sass', function() {
  return gulp.src('scss/app.scss')
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'normal' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('assets/css'));
});

gulp.task('minify-css', function() {
	return gulp.src([
		'assets/css/app.css'])
	    .pipe(cleanCSS())
	    .pipe(concat('style.min.css'))
	    .pipe(gulp.dest('assets/css'));
});

gulp.task('minify-scripts', function() {
  return gulp.src([
        'assets/js/vendor/jquery.js',
		'assets/js/vendor/what-input.js',
        'assets/js/vendor/foundation.js',
        'assets/js/vendor/slick.js',
        'assets/js/vendor/video.min.js',
        'assets/js/vendor/ScrollMagic.min.js',
        'assets/js/vendor/debug.addIndicators.min.js',
        'assets/js/app.js'])
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'))
});

gulp.task('default', ['sass'], function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
});

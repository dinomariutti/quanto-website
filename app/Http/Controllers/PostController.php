<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Tag;
use App\Models\Post;
use App\Models\Category;
use App\Http\Requests\PostStore;
use App\Http\Requests\PostUpdate;
use Illuminate\Foundation\Http\FormRequest;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->paginate(env('PAGINATE', 10));

        /* This will prevent "Pagination gives empty set on non existing page number",
         * especially after deleting a data on the last page
         *
         */
        if (Post::count()) {

            if ($posts->isEmpty()) {

                return redirect()->route('post.index');
            }
        }

        return view('backend.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::get();
        $parent_categories = Category::parentCategory()->get();

        return view('backend.post.create', compact('tags', 'parent_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PostStore  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStore $request)
    {
        if ($post = Post::create($request->all())) {

            // Persist its category, they're always exist (required)
            $post->categories()->attach($request->category_id);

            // Persist its tag, they're always exist (required)
            $post->tags()->attach($request->tag_id);

            // Persist its social SEO, even if they're empty/null
            $post->social()->create([
                'open_graph_title' => $request->open_graph_title,
                'open_graph_sitename' => $request->open_graph_sitename,
                'open_graph_post_url' => $request->open_graph_post_url,
                'open_graph_description' => $request->open_graph_description,
                'open_graph_path' => $request->open_graph_path,
                'open_graph_size' => $request->open_graph_size,
                'open_graph_mime' => $request->open_graph_mime,

                'twitter_card_type' => $request->twitter_card_type,
                'twitter_title' => $request->twitter_title,
                'twitter_description' => $request->twitter_description,
                'twitter_path' => $request->twitter_path,
                'twitter_size' => $request->twitter_size,
                'twitter_mime' => $request->twitter_mime
            ]);

            // If featured image(s) exists, persist
            if ($request->hasFile('images.*.image')) {
                $this->uploadFile($request, $post);
            }
        }

        // If preview action performed, then redirect to a preview page
        if ($post->previewed_at) {
            return redirect()->route('blog.article', $post);
        }

        // Clean up all post previews, if any
        $post->whereNotNull('previewed_at')->delete();

        return redirect()->route('post.index')
                         ->with('success-message', 'New post has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('backend.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $tags = Tag::get();
        $parent_categories = Category::parentCategory()->get();

        return view('backend.post.edit', compact('post', 'tags', 'parent_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PostUpdate  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdate $request, Post $post)
    {
        if ($post->update($request->all())) {

            $post->tags()->sync($request->tag_id);
            $post->categories()->sync($request->category_id);
            
            $post->social()->update([
                'open_graph_title' => $request->open_graph_title,
                'open_graph_sitename' => $request->open_graph_sitename,
                'open_graph_post_url' => $request->open_graph_post_url,
                'open_graph_description' => $request->open_graph_description,
                'open_graph_path' => $request->open_graph_path,
                'open_graph_size' => $request->open_graph_size,
                'open_graph_mime' => $request->open_graph_mime,

                'twitter_card_type' => $request->twitter_card_type,
                'twitter_title' => $request->twitter_title,
                'twitter_description' => $request->twitter_description,
                'twitter_path' => $request->twitter_path,
                'twitter_size' => $request->twitter_size,
                'twitter_mime' => $request->twitter_mime
            ]);
        }

        // If featured image(s) exists, persist
        if ($request->hasFile('images.*.image')) {
            $this->uploadFile($request, $post);
        }

        // If preview action performed, then redirect to a preview page
        if ($post->previewed_at) {
            return redirect()->route('blog.article', $post);
        }

        // Clean up all post previews, if any
        $post->whereNotNull('previewed_at')->delete();

        return redirect()
                ->route('post.index', [
                    'page' => $request->page ?? 1
                ])
                ->with('success-message', 'Post has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        // NOTE: This is a soft delete, no need to remove post's image

        $post->delete();

        return back()->with('success-message', 'Post has been removed.');
    }

    /**
     * Associate a file(s) for a post.
     *
     * @param  \Illuminate\Foundation\Http\FormRequest $request
     * @param  \App\Models\Post $post
     * @return void
     */
    protected function uploadFile(FormRequest $request, Post $post)
    {
        $path = 'post-' . $post->id;

        foreach ($request['images'] as $file) {

            $fileExtension  = $file['image']->getClientOriginalExtension();
            $fileName       = Carbon::now()->format('Ymdhis') . '-' . mt_rand(100, 999) . '.' . $fileExtension;

            $file['image']->storeAs('public/' . $path, $fileName);

            // Persist file(s) to database, and associate them with *this* post
            $post->images()->create([
                'path' => $path . '/' . $fileName,
                'size' => $file['image']->getSize(),
                'mime' => $file['image']->getMimeType()
            ]);
        }
    }
}

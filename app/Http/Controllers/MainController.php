<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Pagination\LengthAwarePaginator;

class MainController extends Controller
{
    public function home()
    {
        $page = 'home';
        $featured_posts  = Post::latest()->skip(0)->take(2)->get();
        $remaining_posts = Post::latest()->skip(2)->take(4)->get();
        
	    return view('pages.home', compact('page', 'featured_posts', 'remaining_posts'));
    }
    
    public function how_it_works()
    {
	    return view('pages.how-it-works', ['page' => "how-it-works"]);
    }
    
    public function sellers()
    {
	    return view('pages.sellers', ['page' => "sellers"]);
    }
    
    public function dealers()
    {
	    return view('pages.dealers', ['page' => "dealers"]);
    }

    public function blog()
    {
        $page = 'blog';
        $categories = Category::get();
        $tags = Tag::whereHas('posts')->get();
        $latest_post = Post::latest()->first();
        
        if (isset($latest_post)) {
            $remaining_posts = Post::where('id', '<>', $latest_post->id)
                                ->latest()
                                ->skip(1)->take(10)
                                ->paginate(9);
        }
	    
        return view('pages.blog', 
                    compact('page', 
                            'tags',
                            'latest_post', 
                            'remaining_posts', 
                            'categories'
                    ));
    }
    
    public function article($id)
    {
        $page = 'blog';
        $categories = Category::get();
        $tags = Tag::whereHas('posts')->get();
        
        $post = Post::find($id);
        
        if (Post::count() <= 3) {
            $more_posts = Post::get()->random(Post::count());
        }
        elseif (Post::count() > 3) {
            $more_posts = Post::where('id', '<>', $post->id)->get()->random(3);
        }
	    
        return view('pages.article', 
                    compact('page',
                            'tags',
                            'post',
                            'more_posts',
                            'categories'
                    ));
    }
    
    public function about_us()
    {
        return view('pages.about-us', ['page' => "about-us"]);
    }
    
    public function contact_us()
    {
        return view('pages.contact-us', ['page' => "contact-us"]);
    }
    
    public function download_app()
    {
        return view('pages.download-app', ['page' => "download-app"]);
    }
    
    public function faq()
    {
        return view('pages.faq', ['page' => "faq"]);
    }
    
    public function tips()
    {
        return view('pages.tips', ['page' => "tips"]);
    }

    public function blogsByTag(Tag $tag)
    {
        $page = 'blog';
        $categories = Category::get();
        $tags = Tag::whereHas('posts')->get();
        $posts = $tag->posts()->latest()->paginate(12);
	    
        return view('pages.blog-by-tag', 
                    compact('page',
                            'tag', 
                            'tags',
                            'categories',
                            'posts'
                    ));
    }

    public function blogsByCategory(Category $category)
    {
        $page = 'blog';
        $categories = Category::get();
        $tags = Tag::whereHas('posts')->get();
        // $posts = $category->posts()->latest()->paginate(9);

        $posts = collect(); // Use this custom collection, instead

        $lv1_posts = $category->posts;
        foreach ($lv1_posts as $lv1_post) {
            $posts->push($lv1_post);
        }

        $childs = $category->childs;
        foreach ($childs as $child) {
            foreach ($child->posts as $lv2_post) {
                $posts->push($lv2_post);
            }
        }

        $posts = new LengthAwarePaginator($posts, count($posts), 12);
	    
        return view('pages.blog-by-category', 
                    compact('page', 
                            'tags',
                            'category',
                            'categories',
                            'posts'
                    ));
    }
}
<?php

namespace App\Http\Controllers;

use App\models\Tag;
use App\Models\Category;
use App\Http\Requests\TagStore;
use App\Http\Requests\TagUpdate;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::paginate(env('PAGINATE', 5));

        // Use alternative content template
        $content_alt = true;

        return view('backend.tag.index',
                    compact(
                        'tags',
                        'content_alt'
                    )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TagStore  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagStore $request)
    {
        Tag::create($request->all());

        return back()->with('success-message', 'New tag has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        $tags     = Tag::paginate(env('PAGINATE', 5));
        $tag_edit = $tag;

        // Use alternative content template
        $content_alt = true;

        return view('backend.tag.index',
                    compact(
                        'tag_edit',
                        'tags',
                        'content_alt'
                    )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TagUpdate  $request
     * @param  \App\models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(TagUpdate $request, Tag $tag)
    {
        $tag->update($request->all());

        return redirect()
                ->route('tag.index', [
                    'page' => $request->page ?? 1
                ])
                ->with('success-message', 'Tag has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return back()->with('success-message', 'Tag has been removed.');
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserStore;
use App\Http\Requests\UserUpdate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(env('PAGINATE', 5));

        // Use alternative content template
        $content_alt = true;

        return view('backend.user.index',
                    compact(
                        'users',
                        'content_alt'
                    )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UserStore  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStore $request)
    {
        User::create($request->all());

        return back()->with('success-message', 'New user has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $users     = User::paginate(env('PAGINATE', 5));
        $user_edit = $user;

        // Use alternative content template
        $content_alt = true;

        return view('backend.user.index',
                    compact(
                        'user_edit',
                        'users',
                        'content_alt'
                    )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UserUpdate  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdate $request, User $user)
    {
        $user->update($request->all());

        return redirect()
                ->route('user.index', [
                    'page' => $request->page ?? 1
                ])
                ->with('success-message', 'User has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return back()->with('success-message', 'User has been removed.');
    }
}

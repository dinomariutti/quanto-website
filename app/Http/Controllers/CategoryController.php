<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\CategoryStore;
use App\Http\Requests\CategoryUpdate;

class CategoryController extends Controller
{
    protected $parentCategories;

    public function __construct()
    {
        $this->parentCategories = Category::with('parent', 'childs')
                                            ->doesntHave('parent')
                                            ->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.category.index', [
            'parentCategories'  => $this->parentCategories,
            'content_alt'       => true, // use alternative content template
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CategoryStore  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStore $request)
    {
        Category::create($request->all());

        return back()->with('success-message', 'New category has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('backend.category.index', [
            'parentCategories'  => $this->parentCategories,
            'category_edit'     => $category,
            'content_alt'       => true,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CategoryUpdate $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdate $request, Category $category)
    {
        $category->update($request->all());

        return redirect()
                ->route('category.index', [
                    'page' => $request->page ?? 1
                ])
                ->with('success-message', 'Category has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        $category->childs()->delete();

        return back()->with('success-message', 'Category has been removed.');
    }
}

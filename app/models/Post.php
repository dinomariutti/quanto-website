<?php

namespace App\Models;

use App\User;
use App\Traits\UuidKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use UuidKey;
    use SoftDeletes;

    protected $hidden = ['id'];

    protected $fillable = [
        'user_id',
        'title',
        'excerpt',
        'body',
        'meta_description',
        'published_at',
        'previewed_at'
    ];

    protected $dates = [
        'deleted_at',
        'published_at',
        'previewed_at'
    ];


    /*************************************** RELATIONSHIP ****************************************/

    /**
     * One-to-Many: A user may create zero or many post
     *
     * This function will retrieve the creator (user) of a post.
     * See: User' posts() method for the inverse
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Many-to-Many: A post may have one or many categories.
     *
     * This function will retrieve the categories of a post.
     * See: Category's posts() method for the inverse
     *
     * @return mixed
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class)
                    ->withTrashed();
    }

    /**
     * Many-to-Many Polymorphic: A post may have one or many tags.
     *
     * This function will retrieve the tags of a post.
     * See: Tag's posts() method for the inverse
     *
     * @return mixed
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Polymorphic: A product may have one or more images.
     *
     * This function will retrieve the images of a product.
     * See: Image's imageable() method
     *
     * @return mixed
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * One-to-One: A post may have zero or one social SEO
     *
     * This function will retrieve the social SEO of a post
     * See: Social' post() method for the inverse
     *
     * @return mixed
     */
    public function social()
    {
        return $this->hasOne(Social::class);
    }

    /**
     * One-to-Many: A post may have zero or many likes
     *
     * This function will retrieve the likes of a post
     * See: Like' post() method for the inverse
     *
     * @return mixed
     */
    public function likes()
    {
        return $this->hasMany(Like::class);
    }


    /***************************************** ACCESSOR ******************************************/

    public function getPublishedAtFormattedAttribute()
    {
        return ($this->published_at) ? 
                    '<strong class="text-green">YES</strong> <br>' .
                    '<span class="text-muted">' . 
                        $this->published_at->format('d-M-Y') . 
                    '<span>' : 
                    '<strong class="text-red">NO</strong>';
    }

    public function getCreatedAtFormattedAttribute()
    {
        return $this->created_at->diffForHumans() . '<br>' .
                '<small class="text-muted">' .
                    $this->created_at->format('d-M-Y') .
                '</small>';
    }

    public function getCategoryListAttribute()
    {
        if ($this->categories->isEmpty()) return '-';

        foreach ($this->categories as $category) {
            echo '<a href="#">' . $category->name . '</a>, ';
        }
    }

    public function getTagListAttribute()
    {
        if ($this->tags->isEmpty()) return '-';
        
        foreach ($this->tags as $tag) {
            echo '<a href="#">' . $tag->name . '</a>, ';
        }
    }

    public function getFeaturedImageAttribute()
    {
        return $this->images()->latest()->first();
    }


    /***************************************** HELPER ********************************************/

    public function toggleLike($user = null) {
        
        if ($user == null) {

            $ip = $_SERVER['REMOTE_ADDR'] ?: 
                    ($_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['HTTP_CLIENT_IP']);

            $this->likes()->updateOrCreate(['ip_address' => $ip]);

        } 
        else {
            
            $this->likes()->updateOrCreate(['user_id' => $user->id]);
        }
    }
}

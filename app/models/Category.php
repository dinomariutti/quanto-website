<?php

namespace App\Models;

use App\Traits\UuidKey;
use App\Scopes\OrderByNameScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use UuidKey;
    use SoftDeletes;

    protected $hidden = ['id'];

    protected $fillable = [
        'name',
        'parent_id'
    ];

    protected $dates = ['deleted_at'];

    
    /******************************************* SCOPE *******************************************/

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrderByNameScope());
    }

    /**
     * Scope a query to only include parent categories
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeParentCategory($query)
    {
        return $query->doesntHave('parent');
    }


    /*************************************** RELATIONSHIP ****************************************/

    /**
     * One-to-Many (self-join): A category may have none or many sub-categories.
     *
     * This function will retrieve the sub-categories of a category, if any.
     * See: Category's parent() method for the inverse
     *
     * @return mixed
     */
    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * One-to-Many (self-join): A category may have none or many sub-categories.
     *
     * This function will retrieve the parent of a sub-categories.
     * See: Category's childs() method for the inverse
     *
     * @return mixed
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id')
                    ->withTrashed();
    }

    /**
     * Many-to-Many: A post may have one or many categories.
     *
     * This function will retrieve the posts of a category.
     * See: Post's categories() method for the inverse
     *
     * @return mixed
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}

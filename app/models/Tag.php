<?php

namespace App\Models;

use App\Traits\UuidKey;
use App\Scopes\OrderByNameScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use UuidKey;
    use SoftDeletes;

    protected $hidden = ['id'];

    protected $fillable = ['name'];

    protected $dates = ['deleted_at'];

    
    /******************************************* SCOPE *******************************************/

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrderByNameScope());
    }


    /*************************************** RELATIONSHIP ****************************************/

    /**
     * Many-to-Many Polymorphic: A post may have one or many tags.
     *
     * This function will retrieve the posts of a tag.
     * See: Post's tags() method for the inverse
     *
     * @return mixed
     */
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'taggable');
    }
}

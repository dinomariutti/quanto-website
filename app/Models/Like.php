<?php

namespace App\Models;

use App\User;
use App\Traits\UuidKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use UuidKey;
    use SoftDeletes;

    protected $hidden = ['id'];

    protected $fillable = [
        'post_id',
        'user_id',
        'ip_address'
    ];

    /*************************************** RELATIONSHIP ****************************************/

    /**
     * One-to-Many: A post may have zero or many likes
     *
     * This function will retrieve the post of a like
     * See: Post' likes() method for the inverse
     *
     * @return mixed
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * One-to-Many: A user may give zero or many likes (to a post)
     *
     * This function will retrieve a user who give a like
     * See: User' likes() method for the inverse
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Models;

use App\Traits\UuidKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use UuidKey;
    use SoftDeletes;

    protected $hidden = ['id'];

    protected $fillable = [
        'path',
        'size',
        'mime',
        'imageable_id',
        'imageable_type'
    ];

    protected $dates = ['deleted_at'];


    /*************************************** RELATIONSHIP ****************************************/

    /**
     * Polymorphic: An image could be owned by a product, a category, etc.
     *
     * This function will get all of the owning imageable models.
     * See: Product|Preview's images() method for the inverse
     *
     * @return mixed
     */
    public function imageable()
    {
        return $this->morphTo();
    }
}

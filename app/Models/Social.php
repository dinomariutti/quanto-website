<?php

namespace App\Models;

use App\Traits\UuidKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Social extends Model
{
    use UuidKey;
    use SoftDeletes;

    protected $hidden = ['id'];

    protected $fillable = [
        'open_graph_title',
        'open_graph_sitename',
        'open_graph_post_url',
        'open_graph_is_use_featured_iamge',
        'open_graph_description',
        'open_graph_path',
        'open_graph_size',
        'open_graph_mime',

        'twitter_card_type',
        'twitter_title',
        'twitter_description',
        'twitter_is_use_featured_image',
        'twitter_path',
        'twitter_size',
        'twitter_mime'
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'open_graph_is_use_featured_iamge' => 'boolean',
        'twitter_is_use_featured_image' => 'boolean'
    ];


    /*************************************** RELATIONSHIP ****************************************/

    /**
     * One-to-One: A post may have zero or one social SEO
     *
     * This function will retrieve the post of a social SEO
     * See: Post' social() method for the inverse
     *
     * @return mixed
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}

<?php

namespace App;

use App\Traits\UuidKey;
use App\Scopes\OrderByNameScope;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use UuidKey;
    use HasRoles;
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 
        'email', 
        'password',
    ];

    protected $hidden = [
        'id', 
        'password', 
        'remember_token',
    ];

    protected $dates = ['deleted_at'];

    /******************************************* SCOPE *******************************************/

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new OrderByNameScope());
    }


    /*************************************** RELATIONSHIP ****************************************/

    /**
     * One-to-Many: A user may create zero or many post
     *
     * This function will retrieve the posts created by a user, if any.
     * See: Post' user() method for the inverse
     *
     * @return mixed
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * One-to-Many: A user may create zero or many post's preview
     *
     * This function will retrieve the creator (user) of a post's preview.
     * See: Preview' user() method for the inverse
     *
     * @return mixed
     */
    public function previews()
    {
        return $this->hasMany(Preview::class);
    }

    /**
     * One-to-Many: A user may give zero or many likes (to a post)
     *
     * This function will retrieve the likes given by a user
     * See: Like' user() method for the inverse
     *
     * @return mixed
     */
    public function likes()
    {
        return $this->hasMany(Like::class);
    }
}
